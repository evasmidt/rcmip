import click


@click.command(context_settings={"help_option_names": ["-h", "--help"]})
@click.option(
    "--src",
    type=click.Path(exists=True, dir_okay=False, readable=True, resolve_path=True),
    help="Plain text file to convert",
    required=True,
)
@click.option(
    "--ncols",
    type=click.INT,
    help="Number of columns in the table.",
    required=True,
)
@click.option(
    "--dst",
    type=click.Path(dir_okay=False, writable=True, resolve_path=True),
    help="File to write output into",
    required=True,
)
def convert_table(src, ncols, dst):
    """Convert plain text into a table"""
    with open(src, "r") as f:
        inlines = f.read().split("\n")

    outlines = []
    outline_no = 0
    for i, inline in enumerate(inlines):
        if i % ncols == 0:
            outlines.append(inline)
        else:
            outlines[outline_no] = "{} & {}".format(outlines[outline_no], inline)

        if i != 0 and i % ncols == (ncols-1):
            outlines[outline_no] = "{} \\\\".format(outlines[outline_no])
            outline_no += 1

    replacements = {
        "https://search.es-doc.org/": "\\url{https://search.es-doc.org/}",
        "%": "\\%",
        "~": "$\\sim$",
        "W m-2": "\\unit{W m^{-2}}",
    }
    output = "\n\\middlehline\n".join(outlines)
    for old, new in replacements.items():
        output = output.replace(old, new)

    with open(dst, "w") as f:
        f.write(output)


if __name__ == "__main__":
    convert_table()
