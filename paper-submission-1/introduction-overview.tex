In an ideal world, sufficient computing power would enable running our most comprehensive, physically complete models for every application of interest.
However, computational limits do exist so for many applications other, less computationally demanding, approaches are used.
One common approach is the use of reduced complexity climate models (RCMs), also known as simple climate models (SCMs).

[TODO add some history here about when RCMs first started popping up and in what form]

[TODO describe spectrum of models in general form to give readers a flavour of kinds of models we're talking about]

As a general rule, RCMs exchange limited spatial and temporal resolution for computational efficiency.
They usually focus on global-mean, annual-mean quantities and, as a result, can be on the order of a million times faster (or more) than more complex models (in terms of simulated model years per unit CPU time).

The computational efficiency of RCMs means that they can be used where computational constraints would otherwise be limiting.
Such cases include performing climate assessment for hundreds to thousands of scenarios, exploring interacting uncertainties from multiple parts of the climate system or combining multiple lines of evidence in an internally consistent setup.
In the context of the Intergovernmental Panel on Climate Change (IPCC), a prominent example is the climate assessment of the Working Group 3 (WGIII) socioeconomic scenarios.
Given there are hundreds of emission scenarios submitted by Integrated Assessment Models in IPCC AR5 and AR6 (available at \url{https://secure.iiasa.ac.at/web-apps/ene/AR5DB} and \url{https://tntcat.iiasa.ac.at/SspDb}, hosted by the IIASA Energy Program), it is unfeasible to assess their climate implications with the world’s most comprehensive models.

At this point, it should be noted that RCMs are used for more than just their computational efficiency.
RCMs also offer conceptual simplicity.
This means they can be used to interpret results from higher complexity models or observations.
This use is well-established in the literature (see e.g. \citet{Held_2010_bjj8vh} and \citet{Gregory_2004_bm82st}) and has even been described as `indispensable' \citep{Rohrschneider_2019_ggfx33}.
Despite this, RCMIP focuses on the use of RCMs as projection tools hence an examination of the conceptual benefits of different RCMs is beyond RCMIP's scope.

When RCMs are used to overcome computational constraints, they are typically used in one of two ways.
The first is `emulation' mode, where the RCMs are run in a setup which has been tuned to reproduce the behaviour of a Coupled Model Intercomparison Project (CMIP) \citep{eyring_2016_fjksdl,Taylor_2012_b7xgw2} model as closely as possible over a range of scenarios.
The second is `probabilistic' mode, where the RCMs are run with a parameter ensemble which captures the uncertainty in historical observations.
Probabilistic climate projections are derived by running an RCM with a parameter ensemble which captures the range of responses consistent with our understanding of the climate system.
The resulting ensemble is designed to capture the likelihood of different warming levels based on the available evidence hence is quite different from an ensemble of multiple model outputs, which have been produced independently.
As a result, these probabilistic distributions are able to provide both best-estimate projections as well as projections at different likelihood levels (e.g. 5\%, 17\%, 83\%, 95\%).
A combination of the two approaches can be used as well.
For example, the MAGICC6 probabilistic setup used in the IPCC’s Fifth Assessment Report \citep{IPCC_2013_WGI_SPM} used randomly drawn emulations for the carbon cycle response whilst using a probabilistic parameter ensemble for the climate response to radiative forcing \citep{Meinshausen_2009_b9j8fj}.

The validity of both approaches rests on the premise that RCMs are able to replicate the response characteristics of the Earth System and our most complete models.
Over time, multiple efforts have been made to evaluate this ability.
In 1997, in an IPCC Technical Paper \citep{houghton_1997_adf9ss} simple climate models used in the IPCC Second Assessment Report were investigated and their performance compared against idealised AOGCM results.
\Citet{Vuuren_2009_bcmqjh} compared the climate components used in integrated assessment models (IAMs), such as DICE or FUND, or the RCM MAGICC4 used in several IAMs.
They focused on five \chem{CO_2}-only experiments to quantify the differences in the behaviour of each IAM’s climate component (each of which is an RCM due to computational constraints).
\citet{Harmsen_2015_f72ghq} extended the work of \citet{Vuuren_2009_bcmqjh} to consider the impact of non-\chem{CO_2} climate drivers in the RCPs.
Recently, \citet{Schwarber_2019_ggfp6r} proposed a series of impulse tests for simple climate models in order to isolate differences in model behaviour under idealised conditions.

Despite these efforts, a detailed evaluation and assessment of RCMs is a clear gap in the research literature.
The Reduced Complexity Model Intercomparison Project (RCMIP) provides a framework to fill these gaps and this overview paper describes how it does so.
One key step is the definition of a clear input and output data handling framework.
Historically, comparing RCMs required a few individuals to learn how to setup, configure and run multiple RCMs in order to produce results.
This required significant time and hence, as previously discussed, has only been attempted a few times.
With a common framework, once a model has participated in RCMIP, it is simpler to run it again in different experiments and provide output in a common, standard format.
This allows researchers to design, run and analyse experiments with far less effort than was previously required.
As a result, it becomes feasible to do much more thorough and regular assessment of RCMs.
This improves our knowledge of RCMs, our understanding of the implications of their quantitative results and our ability to develop and improve them.

RCMIP focuses on RCMs and is not one of the official CMIP6 \citep{eyring_2016_fjksdl} endorsed intercomparison projects that are designed for Earth System Models.
However, RCMIP does replicate selected experimental designs of many of the CMIP-endorsed MIPs, particularly the DECK simulations \citep{eyring_2016_fjksdl}, ScenarioMIP \citep{ONeill_2016_f87n56}, AerChemMIP \citep{Collins_2017_gcc5z6} and others, e.g. ZECMIP \citep{jones_2019_976cds}, DAMIP \citep{Gillett_2016_f89cqb} or PMIP4 \citep{kageyama_2018_fhugcd}.
Hence whilst it is not a CMIP6 endorsed intercomparison, it is related and we hope that our results may be useful beyond the RCM community and are very grateful to all those outside our community whose results we could not do without.

In what follows, we describe the questions RCMIP will aim to address.
In section \ref{sec:science-objectives}, we detail the domain of RCMIP and its scientific objectives.
In section [TODO ref], we examine existing uses of RCMs in the literature and describe how they can be related to RCMIP's science objectives.
In section [TODO ref], we provide an overview of RCMs in the literature before providing an outlook ([TODO section ref]) for RCMIP and conclusions ([TODO section ref]).
