Sufficient computing power to enable running our most comprehensive, physically complete climate models for every application of interest is not available.
Thus, for many applications less computationally demanding approaches are used.
One common approach is the use of reduced complexity climate models (RCMs), also known as simple climate models (SCMs).

RCMs are designed to be computationally efficient tools, allowing for exploratory research and have smaller spatial and temporal resolution than complex models.
Typically, they describe highly parameterised macro properties of the climate system.
Usually this means that they simulate the climate system on a global-mean, annual-mean scale although some RCMs have slightly higher spatial and/or temporal resolutions.
As a result of their highly parameterised approach, RCMs can be on the order of a million or more times faster than more complex models (in terms of simulated model years per unit CPU time).

The computational efficiency of RCMs means that they can be used where computational constraints would otherwise be limiting.
For example, some applications of Integrated Assessment Models (IAMs) require iterative climate simulations.
As a result, hundreds to thousands of climate realisations must be integrated by the IAM for a single scenario to be produced.
RCMs also enable the exploration of interacting uncertainties from multiple parts of the climate system or the constraining of unknown parameters by combining multiple lines of evidence in an internally consistent setup.
In the context of the assessment reports of the Intergovernmental Panel on Climate Change (IPCC), a prominent example is the climate assessment of socioeconomic scenarios by IPCC Working Group 3 (WGIII).
Hundreds of emission scenarios were assessed in the IPCC’s Fifth Assessment Report (AR5, see \citet{IPCC_AR5_WG3_Ch6}) as well as its more recent Special Report on Global Warming of 1.5\degree C (SR1.5, see \citet{IPCC_2018_SR15_Ch_2,SR15_database}).
(Scenario data is available at \url{https://secure.iiasa.ac.at/web-apps/ene/AR5DB} and \url{https://data.ene.iiasa.ac.at/iamc-1.5c-explorer/} for AR5 and SR1.5 respectively, both databases are hosted by the IIASA Energy Program).
For the IPCC’s forthcoming Sixth Assessment (AR6), it is anticipated that the number of scenarios will be in the several hundreds to a thousand (for example, see the full set of scenarios based on the SSPs at \url{https://tntcat.iiasa.ac.at/SspDb}).
Both the number of scenarios and the tight timelines of the IPCC assessments render it infeasible to use the world’s most comprehensive models to estimate the climate implications of these IAM scenarios.

There are two key modes of use which are relevant for the assessment of a large number of IAM scenarios.
The first is `emulation' mode, where the RCMs are run in a setup which has been calibrated to reproduce the behaviour of a Coupled Model Intercomparison Project (CMIP) \citep{eyring_2016_fjksdl,Taylor_2012_b7xgw2} model as closely as possible over a range of scenarios.
The second is `probabilistic' mode, where the RCMs are run with a parameter ensemble which captures the uncertainty in estimates of specific Earth system quantities, be it observations of historical global mean temperature increase, radiative forcing, ocean heat uptake, or cumulative land or ocean carbon uptake.
Probabilistic climate projections are derived by running parametric ensembles of RCM simulations which capture the range of responses consistent with our understanding of the climate system \citep{Meinshausen_2009_b9j8fj,Smith_2018_gdrwm6,Goodwin_2016_gft5nc}.
The resulting ensemble is designed to capture the likelihood that different warming levels are reached under a specific emissions scenario (e.g. 50\% and 66\%) based on the combined available evidence hence is quite different from an ensemble emulating multiple model outputs, which have been produced independently with no relative relationship or probabilities in mind.
The two approaches, emulation of complex models and historically constrained probabilistic mode, can also be combined, e.g. where historical constraints are very weak.
For example, the MAGICC6 probabilistic setup used in AR5 \citep{IPCC_AR5_WG3_Ch6} used randomly drawn emulations for the carbon cycle response whilst using a probabilistic parameter ensemble for the climate response to radiative forcing \citep{Meinshausen_2009_b9j8fj}.

RCMs also play the role of `integrators of knowledge', examining the combined response of multiple interacting components of the climate system.
The most comprehensive RCMs will include (highly parameterised) representations of the carbon cycle, permafrost, non-\chem{CO_2} gas cycles, aerosol chemistry, temperature response to radiative forcing, ocean heat uptake, sea-level rise and all their interactions and feedbacks.
More complex models cannot include as many interactive components without the computational cost quickly becoming prohibitive for running multiple century-long simulations.
As a result, RCMs are able to examine the implications of the Earth System's feedbacks and interactions in a way which cannot be done with other techniques.

\subsection{Evaluation of reduced complexity climate models} % (fold)
\label{sub:evaluation_of_rcms}

The validity of the RCM approach rests on the premise that RCMs are able to replicate the behaviour of the Earth system and response characteristics of our most complete models.
Over time, multiple independent efforts have been made to evaluate this ability.
In 1997, an IPCC Technical Paper \citep{houghton_1997_adf9ss}, investigated the simple climate models used in the IPCC Second Assessment Report and compared their performance with idealised Atmosphere-Ocean General Circulation Model (AOGCM) results.
Later, \Citet{Vuuren_2009_bcmqjh} compared the climate components used in IAMs, such as DICE \citep{Nordhaus_2014}, FUND \citep{waldhoff_2011_marginal} and the RCM MAGICC (version 4 at the time \citep{Wigley_2001}), which is used in several IAMs.
They focused on five \chem{CO_2}-only experiments to quantify the differences in the behaviour of the RCMs used by each IAM.
\citet{Harmsen_2015_f72ghq} extended the work of \citet{Vuuren_2009_bcmqjh} to consider the impact of non-\chem{CO_2} climate drivers in the RCPs.
Recently, \citet{Schwarber_2019_ggfp6r} proposed a series of impulse tests for simple climate models in order to isolate differences in model behaviour under idealised conditions.

Building on these efforts, an ongoing comprehensive evaluation and assessment of RCMs requires an established protocol.
The Reduced Complexity Model Intercomparison Project (RCMIP) proposed here provides such a protocol (also see \url{rcmip.org}).
We aim for RCMIP to provide a focal point for further development and an experimental design which allows models to be readily compared and contrasted.
We believe that a comprehensive, systematic effort will result in a number of benefits seen in other MIPs \citep{carlson_eyring_2017} including building a community of reduced complexity modellers, facilitating comparison of model behaviour, improving understanding of their strengths and limitations, and ultimately also improving RCMs.

RCMIP focuses on RCMs and is not one of the official CMIP6 \citep{eyring_2016_fjksdl} endorsed intercomparison projects that are designed for Earth System Models.
However, RCMIP does replicate selected experimental designs of many of the CMIP-endorsed MIPs, particularly the DECK simulations \citep{eyring_2016_fjksdl}, ScenarioMIP \citep{ONeill_2016_f87n56}, AerChemMIP \citep{Collins_2017_gcc5z6}, C4MIP \citep{jones_c4mip_2016}, ZECMIP \citep{jones_2019_976cds}, DAMIP \citep{Gillett_2016_f89cqb} and PMIP4 \citep{kageyama_2018_fhugcd}.
Hence whilst RCMIP is not a CMIP6 endorsed intercomparison, its design is closely related in the hope that its results may be useful beyond the RCM community.

In what follows, we describe RCMIP Phase 1.
In section \ref{sec:science-objectives}, we detail the domain of RCMIP Phase 1 and its scientific objectives.
In sections \ref{sec:simulation-design} and \ref{sec:output-specifications}, we described the simulations performed and outputs requested from each model.
In section \ref{sec:sample-results} we present sample results from RCMIP Phase 1, before presenting possible extensions to RCMIP Phase 1 and conclusions in sections \ref{sec:extensions} and \ref{sec:conclusions}.

% subsection evaluation_of_rcms (end)
