# Notes from reading RCEMIP overview paper

paper url: https://doi.org/10.5194/gmd-11-793-2018

## Sections

1. Introduction

    1. Introduce the definition and history of RCMs.
    2. Introduce the application of RCMs. Draw out some open questions from the previous research efforts.
    3. Create a niche for this MIP, why we need this MIP. Talk about the advantage of this MIP.
    4. Structure of this paper.

1. Science objectives

    - Create a section to explain why we want to answer these three main scientific questions.

1. Simulation design

    - experiment design and protocol
    - model setup by type/drive mode

1. Output specifications

    - output protocol
    - diagnostics (ECS both ways, TCR, TCRE)

1. Sample results
1. Extensions

    - This part shows the extension of this MIP, which means there are more potentials of this MIP, and it will encourage and give participants a kind of sense how to get involve in this MIP.

1. Conclusions
