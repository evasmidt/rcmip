ID
Drivers (CC=CO2 concentrations, CO=non-CO2 GHG concentrations, EC=CO2 emissions, EO=non-CO2 GHG emissions, A=aerosol emissions, S=solar effective radiative forcing, V=volcanic effective radiative forcing)
Summary
Further information (ESDOC=https://search.es-doc.org/)
Tier
piControl
CC, CO, A, S, V
Pre-industrial control simulation.
ESDOC
1
esm-piControl
EC, CO, A, S, V
Pre-industrial control simulation with zero anthropogenic perturbation to CO2 emissions.
ESDOC
1
esm-piControl-allGHG
EC, EO, A, S, V
Pre-industrial control simulation with zero anthropogenic perturbation to GHG emissions.
RCMIP specific experiment
2
1pctCO2
CC
1 % per year increase in atmospheric CO2 concentrations.
ESDOC
1
1pctCO2-4xext
CC
1 % per year increase in atmospheric CO2 concentrations until atmospheric CO2 concentrations quadruple, constant CO2 concentrations thereafter.
ESDOC
1
1pctCO2-cdr
CC
1 % per year increase in atmospheric CO2 concentrations until atmospheric CO2 concentrations quadruple and then 1% per year decrease in atmospheric CO2 concentrations until CO2 returns to pre-industrial levels, constant thereafter.
ESDOC
2
abrupt-4xCO2
CC
Abrupt quadrupling of atmospheric CO2 concentrations.
ESDOC
1
abrupt-2xCO2
CC
Abrupt doubling of atmospheric CO2 concentrations.
ESDOC
1
abrupt-0p5xCO2
CC
Abrupt halving of atmospheric CO2 concentrations.
ESDOC
1
esm-pi-cdr-pulse
EC
Removal of 100 GtC in a single year from pre-industrial atmosphere, zero CO2 emissions thereafter.
ESDOC
2
esm-pi-CO2pulse
EC
Addition of 100 GtC in a single year from pre-industrial atmosphere, zero CO2 emissions thereafter.
ESDOC
2
esm-bell-1000PgC
EC
Cumulative addition of 1000 PgC following a bell-curved shaped emissions timeseries.
ESDOC
3
esm-bell-2000PgC
EC
Cumulative addition of 2000 PgC following a bell-curved shaped emissions timeseries.
ESDOC
3
esm-bell-750PgC
EC
Cumulative addition of 750 PgC following a bell-curved shaped emissions timeseries.
ESDOC
3
historical
CC, CO, A, S, V
Simulation of 1850-2014.
ESDOC
1
historical-cmip5
CC, CO, A, S, V
Simulation of 1850-2004, matching forcings as estimated in CMIP5.
http://www.pik-potsdam.de/~mmalte/rcps/
2
hist-aer
A
Simulation of 1850-2014 with aerosol emissions only.
ESDOC
3
hist-CO2
CC
Simulation of 1850-2014 with changing CO2 concentrations only.
ESDOC
3
hist-GHG
CC, CO
Simulation of 1850-2014 with changing GHG concentrations only.
ESDOC
3
hist-nat
S, V
Simulation of 1850-2014 with changing natural forcings only.
ESDOC
3
hist-sol
S
Simulation of 1850-2014 with changing solar forcing only.
ESDOC
3
hist-volc
V
Simulation of 1850-2014 with changing volcanic forcing only.
ESDOC
3
ssp119
CC, CO, A, S, V
Low-end scenario reaching radiative forcing ~1.9 W m-2 in 2100 (using the SSP1 socioeconomic storyline).
ESDOC
1
esm-ssp119
EC, CO, A, S, V
As above except CO2 emissions driven.
ESDOC
1
esm-ssp119-allGHG
EC, EO, A, S, V
As above except all GHG emissions driven.
ESDOC
2
ssp126
CC, CO, A, S, V
Update of RCP2.6 based on the SSP1 socioeconomic storyline.
ESDOC
2
esm-ssp126
EC, CO, A, S, V
As above except CO2 emissions driven.
ESDOC
3
esm-ssp126-allGHG
EC, EO, A, S, V
As above except all GHG emissions driven.
ESDOC
3
ssp245
CC, CO, A, S, V
Update of RCP4.5 based on the SSP2 socioeconomic storyline.
ESDOC
2
esm-ssp245
EC, CO, A, S, V
As above except CO2 emissions driven.
ESDOC
3
esm-ssp245-allGHG
EC, EO, A, S, V
As above except all GHG emissions driven.
ESDOC
3
ssp370
CC, CO, A, S, V
Gap-filling scenario reaching radiative forcing ~7.0 W m-2 in 2100 (using the SSP3 socioeconomic storyline).
ESDOC
2
esm-ssp370
EC, CO, A, S, V
As above except CO2 emissions driven.
ESDOC
3
esm-ssp370-allGHG
EC, EO, A, S, V
As above except all GHG emissions driven.
ESDOC
3
ssp370-lowNTCF
CC, CO, A, S, V
Gap-filling scenario reaching radiative forcing ~7.0 W m-2 in 2100 with low near-term climate forcers (using the SSP3 socioeconomic storyline).
ESDOC
2
esm-ssp370-lowNTCF
EC, CO, A, S, V
As above except CO2 emissions driven.
ESDOC
3
esm-ssp370-lowNTCF-allGHG
EC, EO, A, S, V
As above except all GHG emissions driven.
ESDOC
3
ssp370-lowNTCF-gidden
CC, CO, A, S, V
Comparison scenario, follows the ssp370-lowNTCF specifications presented in Gidden et al. 2019 [TODO reference].
RCMIP specific
3
esm-ssp370-lowNTCF-gidden
EC, CO, A, S, V
As above except CO2 emissions driven.
RCMIP specific
3
esm-ssp370-lowNTCF-gidden-allGHG
EC, EO, A, S, V
As above except all GHG emissions driven.
RCMIP specific
3
ssp434
CC, CO, A, S, V
Gap-filling scenario reaching radiative forcing ~3.4 W m-2 in 2100 with low near-term climate forcers (using the SSP4 socioeconomic storyline).
ESDOC
2
esm-ssp434
EC, CO, A, S, V
As above except CO2 emissions driven.
ESDOC
3
esm-ssp434-allGHG
EC, EO, A, S, V
As above except all GHG emissions driven.
ESDOC
3
ssp460
CC, CO, A, S, V
Update of RCP6.0 based on the SSP4 socioeconomic storyline.
ESDOC
2
esm-ssp460
EC, CO, A, S, V
As above except CO2 emissions driven.
ESDOC
3
esm-ssp460-allGHG
EC, EO, A, S, V
As above except all GHG emissions driven.
ESDOC
3
ssp534-over
CC, CO, A, S, V
Overshoot scenario reaching radiative forcing ~3.4 W m-2 in 2100 having followed the ssp585 pathway until 2030 (using the SSP5 socioeconomic storyline).
ESDOC
2
esm-ssp534-over
EC, CO, A, S, V
As above except CO2 emissions driven.
ESDOC
3
esm-ssp534-over-allGHG
EC, EO, A, S, V
As above except all GHG emissions driven.
ESDOC
3
ssp585
CC, CO, A, S, V
Update of RCP8.5 based on the SSP5 socioeconomic storyline.
ESDOC
1
esm-ssp585
EC, CO, A, S, V
As above except CO2 emissions driven.
ESDOC
1
esm-ssp585-allGHG
EC, EO, A, S, V
As above except all GHG emissions driven.
ESDOC
2
rcp26
CC, CO, A, S, V
RCP2.6 (from CMIP5).
http://www.pik-potsdam.de/~mmalte/rcps/
3
esm-rcp26
EC, CO, A, S, V
As above except CO2 emissions driven.
http://www.pik-potsdam.de/~mmalte/rcps/
3
esm-rcp26-allGHG
EC, EO, A, S, V
As above except all GHG emissions driven.
http://www.pik-potsdam.de/~mmalte/rcps/
3
rcp45
CC, CO, A, S, V
RCP4.5 (from CMIP5).
http://www.pik-potsdam.de/~mmalte/rcps/
3
esm-rcp45
EC, CO, A, S, V
As above except CO2 emissions driven.
http://www.pik-potsdam.de/~mmalte/rcps/
3
esm-rcp45-allGHG
EC, EO, A, S, V
As above except all GHG emissions driven.
http://www.pik-potsdam.de/~mmalte/rcps/
3
rcp60
CC, CO, A, S, V
RCP6.0 (from CMIP5).
http://www.pik-potsdam.de/~mmalte/rcps/
3
esm-rcp60
EC, CO, A, S, V
As above except CO2 emissions driven.
http://www.pik-potsdam.de/~mmalte/rcps/
3
esm-rcp60-allGHG
EC, EO, A, S, V
As above except all GHG emissions driven.
http://www.pik-potsdam.de/~mmalte/rcps/
3
rcp85
CC, CO, A, S, V
RCP8.5 (from CMIP5).
http://www.pik-potsdam.de/~mmalte/rcps/
3
esm-rcp85
EC, CO, A, S, V
As above except CO2 emissions driven.
http://www.pik-potsdam.de/~mmalte/rcps/
3
esm-rcp85-allGHG
EC, EO, A, S, V
As above except all GHG emissions driven.
http://www.pik-potsdam.de/~mmalte/rcps/
3
