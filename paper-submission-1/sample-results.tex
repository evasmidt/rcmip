15 models have participated in RCMIP Phase 1 (see Table \ref{tab:rcmip-model-overview} for an overview and links to key description papers).
This is a promising start, demonstrating that the protocol is accessible to a wide range of modelling teams.
We encourage any other interested groups to join further phases of the project.

The groups which have participated have submitted a number of results.
We provide a brief overview of these here to give an initial assessment of the diversity of models which have submitted results to date.
However, this is not intended as a comprehensive comparison or evaluation.

Firstly, we present a comparison of model best-estimates against observational best estimates (Figure \ref{fig:historical-obs}).
Such comparisons are a natural starting point for evaluation of all RCMs.
We see that all the RCMs are able to capture the approximately 1 \degree C of warming seen in the historical observations compared to a pre-industrial reference period \citep{Richardson_2016_f9cgff, Rogelj_2019_gf45fs}.
We also see that all the RCMs include some representation of the impact of volcanic eruptions, most notably the drop in global-mean temperatures after the eruption of Mount Agung in 1963.
The exception is the \chem{CO_2}-only model, GREB, which lacks the volcanic and aerosol induced cooling signals of the 19\textsuperscript{th} and 20\textsuperscript{th} Centuries.

Another way to evaluate RCMs is to compare their probabilistic results to observational best estimates as well as uncertainties (Figure \ref{fig:probabilistic-projections}).
Such comparisons are vital to understanding the limits of projected probabilistic ranges and their dependence on model structure.
Here we see large differences in probabilistic projections despite the similarities in the models' historical simulations.
Determining the underlying causes of such differences requires investigation into and understanding of how the probabilistic distributions are created.

RCMIP also facilitates a comparison of model calibrations and CMIP output (Figure \ref{fig:emulation-comparison-example}).
Each RCM is calibrated to a different number of CMIP models (some RCMs provide no calibrations at all) because there is no common resource of calibration data.
Instead, the CMIP models to which each RCM is calibrated depends on each RCM development team’s capability and the time at which they last accessed the CMIP archives.

Examining multiple emulation setups (Figures \ref{fig:emulation-comparison-example-CanESM5-r1i1p2f1} - \ref{fig:emulation-comparison-example-CNRM-CM6-1-r1i1p1f2}), RCMs can reproduce the temperature response of CMIP models to idealised forcing changes to within a root-mean square error of 0.2\degree C (Table \ref{tab:rcmip-emulation-scores}).
In scenario-based experiments, it appears to be harder for RCMs to emulate CMIP output than in idealised experiments.
We suggest two key explanations.
The first is that effective radiative forcing cannot be easily diagnosed in SSP scenarios hence it is hard to know how best to force the RCM during calibration.
The second is that the forcing in these scenarios includes periods of increase, sudden decrease due to volcanoes as well as longer term stabilisation rather than the simpler changes seen in the idealised experiments.
Fitting all three of these regimes is a more difficult challenge than fitting the the idealised experiments alone.

We also present plots of the relationship between surface air temperature change and cumulative \chem{CO_2} emissions from the 1pctCO2 and 1pctCO2-4xext experiments (Figure \ref{fig:tcre-plot}).
These can be used to derive the transient climate response to emissions \citep{IPCC_2018_SR15_Glossary}, a key metric in the calculation of our remaining carbon budget \citep{Rogelj_2019_gf45fs}.
The illustrative results here demonstrate a range of relationships between these two key variables, from weakly sub-linear to weakly super-linear (see further discussion in \citet{nicholls_carbon_budget_implications_2020}).

Finally, we present initial results from running both CMIP5 and CMIP6 generation scenarios (`RCP' and `SSP-based' scenarios respectively) with the same models (Figure \ref{fig:rcp-ssp-comparison}).
In the small selection of models which have submitted all RCP, SSP-based scenario pairs, the SSP-based scenarios are 0.21\degree C (standard deviation 0.10\degree C across the models' default setups) warmer than their corresponding RCPs (Figure \ref{fig:rcp-ssp-comparison}(b)).
This difference is driven by the 0.42 $\pm 0.26$ \unit{W m^{-2}} larger effective radiative forcing in the SSP-based scenarios (Figure \ref{fig:rcp-ssp-comparison}(d)), which itself is driven by the larger \chem{CO_2} effective radiative forcing in the SSP-based scenarios (Figure \ref{fig:rcp-ssp-comparison}(f)).
As noted previously, these are only initial results, not a comprehensive evaluation and should be treated as such.
Nonetheless, they agree with other work \citep{Wyser_2020} which suggests that even when run with the same model (in a concentration-driven setup), the SSP-based scenarios result in (non-trivially) warmer projections than the RCPs.
