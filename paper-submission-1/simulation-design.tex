RCMIP Phase 1 includes over 50 experiments.
To help modelling groups prioritise model runs and ensure comparibility of core experiments three tiers of model runs and output variables were defined.
Ideally at least all Tier 1 scenarios and variables for a default model version should be submitted.
The following describes the simulation design, model runs as well as data sources and format of RCMIP.

\subsection{Model configuration} % (fold)
\label{sub:model_configuration}

RCMs are usually highly flexible.
Their response to anthropogenic and natural drivers strongly depends on the configuration in which they are run (i.e. their parameter values).
To mitigate this as a cause of difference between models in RCMIP Phase 1, we have requested that all models provide one set of simulations in which their equilibrium climate sensitivity is equal to 3\degree C.
While this does not define the entirety of a model's behaviour, it removes a major cause of difference between model output which is not related to model structure.
On top of these 3\degree C climate sensitivity simulations, we have also invited groups to submit other default configurations, where each participating modelling group is free to choose their own defaults.
In practice, these defaults are typically a group’s most likely parameter values given their own expert judgement.
Finally, where available, we have also requested probabilistic output i.e. output which quantifies the probable range of a number of output variables rather than a single timeseries for each output variable (see section \ref{sec:introduction}).

% subsection model_configuration (end)

\subsection{RCM drivers} % (fold)
\label{sub:rcm_run_modes}

Depending on the experiment in RCMIP, the drivers of the RCMs will vary e.g. the RCMs might run with prescribed \chem{CO_2} concentrations and calculate consistent \chem{CO_2} emissions or the opposite i.e. run with prescribed \chem{CO_2} emissions and calculate consistent \chem{CO_2} concentrations.
Below we describe each of the different setups used in RCMIP.
However, a model did not need to be able to run in all of these ways to participate in RCMIP Phase 1.

\subsubsection{Concentration driven} % (fold)
\label{ssub:concentration_driven}

The concentration driven setup can strictly better be described as `well-mixed greenhouse gas concentration' driven.
Here, `well-mixed greenhouse gases' refers to \chem{CO_2}, \chem{CH_4}, \chem{N_2O}, hydrofluorocarbons (HFCs), perfluorocarbons (PFCs) and hydrochlorofluorocarbons (HCFCs).
Depending on the experiment, these simulations are also supplemented by aerosol emissions and natural effective radiative forcing (specifically solar and volcanic forcings).
For models which do not include the aerosol emissions to effective radiative forcing step, prescribed aerosol effective radiative forcing can instead be used.

This setup mirrors the majority of experiments performed in CMIP5 and CMIP6 such as the historical, RCP/SSP-based scenario and one percent per year rise in atmospheric \chem{CO_2} concentration (1pctCO2) experiments.
The key difference between the RCMIP experiments and the CMIP experiments is that some RCMs include more anthropogenic drivers than CMIP models.
Specifically, CMIP models do not include the full range of HFC, PFC and HCFC species, instead using equivalent concentrations \citep{Meinshausen_CMIP6_concs_2017,Meinshausen_2019_gf86j8}.
In addition, some CMIP models will not include the effect of aerosol precursors such as nitrates, ammonia and organic carbon \citep{mccoy_aerosol_cloud_2017}.

% subsubsection concentration_driven (end)

\subsubsection{\chem{CO_2} emissions driven} % (fold)
\label{ssub:co2_emissions_driven}

In the \chem{CO_2} emissions driven setup \chem{CO_2} emissions are amended with concentrations of non-\chem{CO_2} well-mixed greenhouse gases.
Like the concentration-driven setup, these simulations are also supplemented by aerosol emissions (or aerosol effective radiative forcing) and natural effective radiative forcings.

This setup mirrors the \chem{CO_2} emissions driven experiments performed in CMIP5 and CMIP6 such as the esm-hist, esm-ssp/rcp and esm-1pctCO2 experiments. As above, a cause of difference between CMIP and RCMIP simulations is the number of climate drivers that are explicitly modelled.

% subsubsection co2_emissions_driven (end)

\subsubsection{Emissions driven} % (fold)
\label{ssub:emissions_driven}

The emissions driven or rather `well-mixed greenhouse gas emissions' driven setup is, like the concentration-driven and \chem{CO_2} emissions driven setups, supplemented by aerosol emissions (or aerosol effective radiative forcing) and natural effective radiative forcings.

These experiments have no obvious equivalent within the CMIP protocol.
However, for many climate policy applications they are the most relevant set of experiments, given that anthropogenic emissions and reduction targets are what climate policy is directly concerned with (rather than atmospheric concentrations of GHGs).
In addition, these experiments are of particular interest to the Integrated Assessment Modelling Consortium (IAMC) community and their contribution in IPCC WGIII because they require climate assessment of socioeconomic scenarios that are described in terms of their corresponding emissions, not concentrations.

% subsubsection emissions_driven (end)

% subsection rcm_run_modes (end)

\subsection{Experimental design} % (fold)
\label{sub:experimental_design}

RCMIP's experimental design focuses on a limited set of the CMIP6 experiment protocol \citep{eyring_2016_fjksdl} plus some CMIP5 experiments \citep{Taylor_2012_b7xgw2}.
We then complement this CMIP-based set with other experiments of interest to the RCM and IAMC communities.

Systematic intercomparison projects such as RCMIP require the definition of a clear input and output data handling framework (see Section \ref{sec:output-specifications} for output specifications).
Historically, comparing RCMs required learning how to set up, configure and run multiple RCMs in order to produce results.
This required significant time and hence, as previously discussed, has only been attempted in standalone cases with a limited number of models \citep{houghton_1997_adf9ss,Vuuren_2009_bcmqjh,Harmsen_2015_f72ghq,Schwarber_2019_ggfp6r}.
With a common framework, once a model has participated in RCMIP, it is simpler to run it again in different experiments and provide output in a common, standardised format.
This allows researchers to design, run and analyse experiments with far less effort than was previously required.
As a result, it becomes feasible to do more regular and targeted assessment of RCMs.
This capacity improves our knowledge of RCMs, our understanding of the implications of their quantitative results and our ability to develop and improve them.

Our input protocol is designed to be easy to use and hence easily able to be extended within future RCMIP phases or in separate research.
The full set of RCMIP experiments is described in Supplementary Table \ref{tab:rcmip-experiment-overview} and available at \url{rcmip.org}.

\subsubsection{Input format} % (fold)
\label{ssub:input_format}

All input data is provided in a text-based format based on the specifications used by the IAMC community \citep{Gidden_2019_ggfx68}.
The computational simplicity of RCMs means that their input specifications are relatively lightweight and hence using an uncompressed, text-based input format is possible.
Further, the format is explicit about associated metadata and ensures metadata remains attached to the timeseries.
As the IAMC community is a major user of RCMs, as well as being the source of input data for many experiments run with RCMs, using their data format ensures that data can be shared easily and assessment of IAM emissions scenarios can be performed with minimal data handling overhead.

The inputs are formatted as text files with comma separated values (CSV), with each row of the CSV file being a timeseries (see \url{rcmip.org}).
This format is also often referred to as `wide' although this term is imprecise \citep{wickham_tidy_data_2014}.
The columns provide metadata about the timeseries, specifically the timeseries' variable, units, region, model and scenario.
Other columns provide the values for each timestep within the timeseries.

Being simplified models, RCMs typically do not take gridded input.
Hence we use a selection of highly aggregated socio-economic regions, which once again follow IAMC conventions \citep{Gidden_2019_ggfx68}.
RCMIP's variables and units are described in Section \ref{sub:variables}.
The regions used in RCMIP are described in Table \ref{tab:rcmip-region-overview}.
Scenarios are discussed in section \ref{ssub:scenario_experiments} and summarised in Table \ref{tab:rcmip-experiment-overview}.

One complication of using the IAMC format is that the `model' column is reserved for the name of the integrated assessment model which produced the scenario.
To enhance compatibility with the IAMC format, we don't use the `model' column.
Instead, as described in Section \ref{sec:output-specifications}, we use the separate `climate\_model' column to store metadata about the climate model which provided the timeseries.

In general, we follow the naming conventions provided by the CMIP6 protocol \citep{eyring_2016_fjksdl}.
These typically specify \chem{CO_2}-emissions driven runs by prefixing the scenario name with `esm-', with all other scenarios being concentration-driven.
Where it is not possible to follow CMIP6 naming schemes, we use our own custom conventions.
For example, full greenhouse gas emissions driven runs are typically not performed in CMIP6 because of computational cost.
RCMIP's convention is to denote all greenhouse gas emissions driven by prefixing the scenario name with `esm-' as well as suffixing the name with `-allGHG' (e.g. `esm-ssp245-allGHG').
In addition, RCMIP includes a number of CMIP5 experiments, which sometimes have the same name as their CMIP6 counterpart (e.g. `historical').
Where such a clash exists, we append the CMIP5 experiment with `-cmip5' to distinguish the two (e.g. `historical-cmip5').
Finally, if an experiment is not a CMIP6-style experiment then we cannot use a CMIP6 name for it.
In such cases, we choose our own name and describe it within Table \ref{tab:rcmip-experiment-overview}.

% subsubsection input_format (end)

\subsubsection{Idealised experiments} % (fold)
\label{ssub:idealised_experiments}

The first group of experiments in RCMIP is idealised experiments.
They focus on examining model response in highly idealised experiments.
These experiments provide an easy point of comparison with output from other models, particularly CMIP output, as well as information about basic model behaviour and dynamics which can be useful for understanding the differences between models.

RCMIP's Tier 1 idealised experiments are: piControl, esm-piControl, 1pctCO2, 1pctCO2-4xext, abrupt-4xCO2, abrupt-2xCO2 and abrupt-0p5xCO2 (Table \ref{tab:rcmip-experiment-overview}).
The piControl and esm-piControl control experiments serve as a useful check of model type.
Most RCMs are perturbation models and hence do not include any internal variability, so will simply return constant values in their control experiments.
Deviations from constant values in the control experiments quickly reveals those models with more complexity.
Apart from esm-piControl, all of the Tier 1 experiments are concentration driven.

After the control experiments, the other Tier 1 experiments examine the models' responses to idealised, \chem{CO_2}-only concentration changes.
They reveal differences in model response to forcing, particularly whether the RCM response to forcing includes non-linearities.
In addition, these experiments also provide a direct comparison with CMIP experiments (i.e. more complex model behaviour) and are a key benchmark when examining an RCM's ability to emulate more complex models.

The idealised Tier 2 experiments add idealised \chem{CO_2} removal experiments, which complement the typically rising/abruptly changing Tier 1 experiments.
Idealised Tier 3 experiments examine the carbon cycle response in more detail with idealised emissions driven experiments as well as experiments in which the carbon cycle is only coupled to the climate system radiatively or biogeochemically (the `1pctCO2-rad' and `1pctCO2-bgc' experiments \citep{jones_c4mip_2016}).
In concentration-driven experiments, RCMs report emissions (often referred to as `inverse emissions’) and carbon cycle behaviour consistent with the prescribed \chem{CO_2} pathway.
For brevity, we do not go through all Tier 2 and 3 experiments in detail here, further information can be found in Table \ref{tab:rcmip-experiment-overview}.


% subsubsection idealised_experiments (end)

\subsubsection{Scenario experiments} % (fold)
\label{ssub:scenario_experiments}

In addition to the idealised experiments, RCMIP also includes a number of scenario based experiments.
These examine model responses to historical transient forcing as well as a range of future scenarios.
The historical experiments provide a way to compare RCM output against observational data records, and are complementary to the idealised experiments which provide a cleaner assessment of model response to forcing.
The future scenarios probe RCM responses to a range of possible climate futures, both continued warming as well as stabilisation or overshoots in forcing.
The variety of scenarios is a key test of model behaviour, evaluating them over a range of conditions rather than only over the historical period.
Direct comparison with CMIP output then provides information about the extent to which the simplifications involved in RCM modelling are able to reproduce the response of our most advanced, physically-based models.

RCMIP's Tier 1 scenario experiments are: historical, ssp119, ssp585, esm-hist, esm-ssp119, esm-ssp585, esm-hist-allGHG, esm-ssp119-allGHG and esm-ssp585-allGHG.
We focus on simulations (historical plus future) which cover the highest forcing (ssp585) and lowest forcing (ssp119) scenarios from the CMIP6 ScenarioMIP exercise \citep{ONeill_2016_f87n56}.
These quickly reveal differences in model projections over the widest available scenario range which can also be compared to CMIP6 output.

The Tier 2 experiments expand the CMIP6 scenario set to include the full range of ScenarioMIP concentration-driven experiments \citep{ONeill_2016_f87n56}, which examine scenarios between the two extremes of ssp585 and ssp119, as well as the CMIP5 historical experiments.
The CMIP5 experiments are particularly useful as they provide a direct comparison between CMIP5 and CMIP6, something which has only been done to a limited extent with more complex models \citep{wyser_2019_scojd2}.
Finally, the Tier 3 experiments add the remaining emissions-driven ScenarioMIP experiments, the rest of the CMIP5 scenario experiments (the so-called `RCPs') and detection and attribution experiments \citep{Gillett_2016_f89cqb} designed to examine the response to specific climate forcers over both the historical period and under a middle of the road emissions scenario (ssp245).

% subsubsection scenario_experiments (end)

\subsubsection{Data sources} % (fold)
\label{ssub:data_sources}

CMIP6 emissions projections follow \citet{Gidden_2019_gf8sgm} and are available at \url{https://tntcat.iiasa.ac.at/SspDb/dsd?Action=htmlpage&page=60} (hosted by IIASA).
Where well-mixed greenhouse gas emissions are missing, we use inverse emissions based on the CMIP6 concentrations from MAGICC7.0.0 \citep{Meinshausen_2019_gf86j8}.
Where regional emissions information is missing, we use the downscaling procedure described in \citet{Meinshausen_2019_gf86j8}.
The emissions extensions also follow the convention described in \citet{Meinshausen_2019_gf86j8}.

For CMIP6 historical emissions (year 1850-2014), we have used data sources which match the harmonisation used for the CMIP6 emissions projections.
This ensures consistency with CMIP6, although it means that we do not always use the latest data sources.
CMIP6 historical anthropogenic emissions for \chem{CO_2}, \chem{CH_4}, BC, CO, \chem{NH_3}, NOx, OC, \chem{SO_2} and non-methane volatile organic compounds (NMVOCs) come from CEDS \citep{Hoesly_2018_gcxtgm}.
Biomass burning emissions data for \chem{CH_4}, BC, CO, \chem{NH_3}, NOx, OC, \chem{SO_2} and NMVOCs come from UVA \citep{Marle_2017_gbxdd4}.
The biomass burning emissions are a blend of both anthropogenic and natural emissions, which could lead to some inconsistency between RCMs as they make different assumptions about the particular anthropogenic/natural emissions split.
\chem{CO_2} global land-use emissions are taken from the Global Carbon Budget 2016 \citep{Quere_2016_bs3k}.
Emissions of \chem{N_2O} and the regional breakdown of \chem{CO_2} land-use emissions come from PRIMAP-hist Version 1.0 \citep[][see \url{https://doi.org/10.5880/PIK.2016.003}]{Guetschow_2016_f9cwhz}.
Where required, historical emissions were extended back to 1750 by assuming a constant relative rate of decline based on the period 1850-1860 (noting that historical emissions are somewhat uncertain, we require consistent emissions inputs in Phase 1, uncertainty in historical emissions will be explored in future research).

CMIP6 concentrations follow \citet{Meinshausen_2019_gf86j8}.
CMIP6 radiative forcings follow the data provided at \url{https://doi.org/10.5281/zenodo.3515339)}.
CMIP5 emissions, concentrations and radiative forcings follow \citet{Meinshausen_2011_fn3pw6} and are taken from \url{http://www.pik-potsdam.de/~mmalte/rcps/}.

% subsubsection data_sources (end)

% subsection experimental_design (end)
