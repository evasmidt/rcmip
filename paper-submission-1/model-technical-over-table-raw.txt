Model
Development home (github, gitlab, internal only etc.)
License
Approximate release frequency
Codebase size (approximate lines of code)
Simulation years per second (approximate for a single core machine)
Language, distribution format and support platforms
Code testing (continuous integration, unit tests etc.)
ACC2
Internal only


50000
100
GAMS
Manual validation and verification
AR5IR
N/A
AGPLv3
N/A
500
5 000
Python 3.7 (\url{https://github.com/openclimatedata/openscm/blob/ar5ir-notebooks/notebooks/ar5ir_rcmip.ipynb})
Manual validation and verification
Cicero-SCM
Internal only


3000


Fortran 90
ESCIMO

GNU GPLv3



Vensim

FaIR v1.5
GitHub
Apache 2.0
Sub-yearly
3 500
3 500
Python 2.7, 3.5, 3.6, 3.7. Linux, macOS and Windows.
Unit tests and continuous integration.
GIR
Github
Creative Commons Attribution 4.0 License

250
200 000
Python 3.6+ (recommended), Excel, MatLAB and IDL available upon request.  Linux, macOS and Windows.
Manual validation and verification
GREB v1.0.1
GitHub
Creative Commons Attribution 4.0 License
Yearly
1700
1
Fortran 90 (GrADS or Python 3.7 for data processing)
Fortran 90 code tested by gfortran on Mac and ifort on Linux under continuous integration
Hector

GNU GPLv3



R and Python packages (with C++ backend). Linux, macOS and Windows.
Unit tests and continuous integration
Held two layer model uom
GitHub
AGPLv3
Yearly
500
5 000
Python 3.7 (\url{https://github.com/openclimatedata/openscm/blob/ar5ir-notebooks/notebooks/held_two_layer_rcmip.ipynb})
Manual validation and verification
MAGICC7.1.0
Private GitLab repository
Custom Community non-commercial license
Decadal (internally approximately monthly, moving to approximately annual public releases)
15 000
350
Fortran 90 (Open-source Python wrapper available). Linux, macOS and Windows.
Unit tests and continuous integration
MCE
Private GitHub, preparing public release
n/a
n/a
760 excluding calibration code
500
Python 2.7 and 3.7 on multiple platforms
Comparing results for idealized scenarios with analytical solutions
OSCAR v3.0
Develoment internal, with release on GitHub \url{https://github.com/tgasser/OSCAR}
CeCILL
sub-yearly
5000 excluding calibration code

Python 3.7
Manual validation and verification
WASP

Creative Commons Attribution License




WASP C++ code tested only on GNU GCC compiler collection
