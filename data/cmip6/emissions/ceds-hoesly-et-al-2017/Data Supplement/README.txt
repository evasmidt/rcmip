This file contains supplementary data to the article:

Historical (1750 – 2014) anthropogenic emissions of reactive gases and aerosols from the Community Emissions Data System (CEDS)

Rachel M. Hoesly
Steven J. Smith
Leyang Feng
Zbigniew Klimont
Greet Janssens-Maenhout
Tyler Pitkanen
Jonathan J. Seibert
Linh Vu
Robert J. Andres
Ryan M. Bolt
Tami C. Bond
Laura Dawidowski
Nazar Kholod
Jun-ichi Kurokawa
Meng Li
Liang Liu
Zifeng Lu
Maria Cecilia P. Moura
Patrick R. O’Rourke
Qiang Zhang


Citation:
Hoesly, R. M., Smith, S. J., Feng, L., Klimont, Z., Janssens-Maenhout, G., Pitkanen, T., Seibert, J. J., Vu, L., Andres, R. J., Bolt, R. M., Bond, T. C., Dawidowski, L., Kholod, N., Kurokawa, J.-I., Li, M., Liu, L., Lu, Z., Moura, M. C. P., O'Rourke, P. R., and Zhang, Q.: Historical (1750–2014) anthropogenic emissions of reactive gases and aerosols from the Community Emissions Data System (CEDS), Geosci. Model Dev., 11, 369-408, https://doi.org/10.5194/gmd-11-369-2018, 2018. 

*************************
*** THIS FILE CONTAINS ADDITIONAL FILES THAN THE SUPPLEMENTAL INFORMATION ORIGINALLY RELEASED WITHT THE JOURNAL ARTICLE ***
* Methane and methane extension were not originally released
* CO2 were not originally released
* For all emission species, small countries/regions/islands were originally released in aggregate regions (i.e. Other Asia). This file contains all countries/regions/islands and does not aggregate to such regions.
*************************

Contents:

- This README file.

- 3 data files each for 9 emissions species and methane extension (SO2, NOx, BC, OC, NH3, NMVOC, CO, CO2, CH4, CH4 extension) including global emissions by sector, total emissions by country, and emissions by sector and country (region, sector, and region and sector for CH4 extension). 30 data files total.

- One file country_mapping.csv contains a mapping of country names to 3 digit ISO codes, IEA country name/region, and aggregate region used in the figures in this paper. 

Note that country totals in these summary files do not include international shipping or aircraft emissions. International shipping and aircraft emissions are reported under the "global" iso.

To receive updates you can sign up for an information distribution list. (This list will be used only for CEDS-related information and e-mail addresses will not be shared or distributed.) To subscribe to the listserv, please send an email to listserv@listserv.umd.edu with the email body: “subscribe cedsinfo”. You can also go directly to the website for the listserv at: https://listserv.umd.edu/archives/cedsinfo.html. You do not have to be registered to view the archive on the website.

Note that known issues with the data are listed at https://github.com/JGCRI/CEDS and users can also submit issues via the GitHub site.

Further Information: rachel.hoesly@pnnl.gov, ssmith@pnnl.gov


