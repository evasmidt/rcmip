import glob
import os.path

from scmdata import ScmDataFrame
import tqdm

for f in tqdm.tqdm(glob.glob("/Users/znicholls/Desktop/MAGICC7.1.0.beta*.csv")):
    tmp = ScmDataFrame(f)
    tmp = tmp.filter(scenario="*early*", keep=False)
    print(os.path.basename(f))
    tmp.to_csv(os.path.basename(f))

