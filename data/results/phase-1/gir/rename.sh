#!/bin/bash
for f in ./*.csv; do
    echo ${f}
    tmp=${f#*GIR_}
    key=${tmp%_data.csv}
    echo "rcmip_phase-1_gir-${key}_v1-0-0.csv"
    mv -v ${f} "rcmip_phase-1_gir-${key}_v1-0-0.csv"
done
