#!/bin/bash
for f in ./*ENS*.csv; do
    echo ${f}
    tmp=${f#*FaIR-1.5-ENS_}
    key=${tmp%.csv}
    tmpmodel=${f%%_*}
    modelcfg=${tmpmodel#./}
    modelcfg=$(echo "${modelcfg}" | tr '[:upper:]' '[:lower:]')
    echo ${key}
    echo ${modelcfg}
    echo "rcmip_phase-1_${modelcfg}-${key}_v1-0-0.csv"
    mv -v ${f} "rcmip_phase-1_${modelcfg}-${key}_v1-0-0.csv"
done


# rcmip_phase-1_magicc7_v1-0-0.csv
