---- HEADER ----

Date: 2019-12-19 12:35:34
Contact: Zebedee Nicholls <zebedee.nicholls@climate-energy-college.org>, Jared Lewis <jared.lewis@climate-energy-college.org>, Malte Meinshausen <malte.meinshausen@unimelb.edu.au>
Source data crunched with: NetCDF-SCM v2.0.0-beta.6 (more info at gitlab.com/znicholls/netcdf-scm)
File written with: pymagicc v2.0.0-beta.8 (more info at github.com/openclimatedata/pymagicc)


---- METADATA ----

CDI: Climate Data Interface version 1.5.4 (http://code.zmaw.de/projects/cdi)
CDO: Climate Data Operators version 1.5.4 (http://code.zmaw.de/projects/cdo)
Conventions: CF-1.5
area_world (m**2): 509499402446956.3
area_world_el_nino_n3.4 (m**2): 6167472062465.617
area_world_land (m**2): 146401455477158.16
area_world_north_atlantic_ocean (m**2): 39450812175826.164
area_world_northern_hemisphere (m**2): 254749701223478.12
area_world_northern_hemisphere_land (m**2): 98593720745412.48
area_world_northern_hemisphere_ocean (m**2): 156155980477608.56
area_world_ocean (m**2): 363097946996914.44
area_world_southern_hemisphere (m**2): 254749701223478.12
area_world_southern_hemisphere_land (m**2): 47807734731745.65
area_world_southern_hemisphere_ocean (m**2): 206941966519305.8
associated_files: baseURL: http://cmip-pcmdi.llnl.gov/CMIP5/dataLocation gridspecFile: gridspec_atmos_fx_bcc-csm1-1_abrupt4xCO2_r0i0p0.nc areacella: areacella_fx_bcc-csm1-1_abrupt4xCO2_r0i0p0.nc
branch_time: 160.0
calendar: 365_day
cmor_version: 2.5.6
comment: Impose an instantaneous quadrupling of CO2, then hold fixed. Other forcings are fixed at the values of pre-industrial control run. The model initial state is from the pre-industrial control simulation at 1st Jul. of year 160.
contact: Dr. Tongwen Wu (twwu@cma.gov.cn)
creation_date: 2011-07-20T01:29:54Z
crunch_contact: Zebedee Nicholls <zebedee.nicholls@climate-energy-college.org>, Jared Lewis <jared.lewis@climate-energy-college.org>, Malte Meinshausen <malte.meinshausen@unimelb.edu.au>
crunch_netcdf_scm_version: 2.0.0-beta.6 (more info at gitlab.com/znicholls/netcdf-scm)
crunch_source_files: Files: ['/cmip5/abrupt4xCO2/Amon/tas/bcc-csm1-1/r7i1p1/tas_Amon_bcc-csm1-1_abrupt4xCO2_r7i1p1_016007-016512.nc']
experiment: abrupt 4XCO2
experiment_id: abrupt4xCO2
forcing: GHG
frequency: mon
history: 2011-07-20T01:29:54Z altered by CMOR: Treated scalar dimension: 'height'.
initialization_method: 1
institute_id: BCC
institution: Beijing Climate Center(BCC),China Meteorological Administration,China
land_fraction: 0.287343723611923
land_fraction_northern_hemisphere: 0.3870219288654692
land_fraction_southern_hemisphere: 0.18766551835837683
model_id: bcc-csm1-1
modeling_realm: atmos
original_name: TREFHT
parent_experiment: pre-industrial control
parent_experiment_id: piControl
parent_experiment_rip: r1i1p1
physics_version: 1
product: output
project_id: CMIP5
realization: 7
table_id: Table Amon (11 April 2011) 1cfdc7322cf2f4a32614826fab42c1ab
title: bcc-csm1-1 model output prepared for CMIP5 abrupt 4XCO2
tracking_id: ba53d683-2488-4305-99c7-d176c9ec9837

&THISFILE_SPECIFICATIONS
    THISFILE_DATACOLUMNS = 11
    THISFILE_DATAROWS = 6
    THISFILE_FIRSTYEAR = 160
    THISFILE_LASTYEAR = 165
    THISFILE_ANNUALSTEPS = 1
    THISFILE_DATTYPE = 'MAG'
    THISFILE_REGIONMODE = 'NONE'
    THISFILE_TIMESERIESTYPE = 'AVERAGE_YEAR_MID_YEAR'
    THISFILE_FIRSTDATAROW = 76
/

   VARIABLE                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas                 tas
       TODO                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET                 SET
      UNITS                   K                   K                   K                   K                   K                   K                   K                   K                   K                   K                   K
      YEARS               WORLD                 N34                LAND                 AMV                  NH              NHLAND             NHOCEAN               OCEAN                  SH              SHLAND             SHOCEAN
        160         2.88242e+02         2.99905e+02         2.83273e+02         2.94045e+02         2.90233e+02         2.85550e+02         2.93190e+02         2.90246e+02         2.86251e+02         2.78576e+02         2.88024e+02
        161         2.88525e+02         2.98949e+02         2.83002e+02         2.93159e+02         2.88999e+02         2.84429e+02         2.91885e+02         2.90751e+02         2.88050e+02         2.80060e+02         2.89896e+02
        162         2.88955e+02         3.00277e+02         2.83513e+02         2.93367e+02         2.89532e+02         2.84970e+02         2.92412e+02         2.91149e+02         2.88379e+02         2.80509e+02         2.90197e+02
        163         2.89349e+02         3.00772e+02         2.84182e+02         2.93775e+02         2.90081e+02         2.85768e+02         2.92805e+02         2.91432e+02         2.88616e+02         2.80912e+02         2.90396e+02
        164         2.89602e+02         3.01083e+02         2.84582e+02         2.93927e+02         2.90431e+02         2.86199e+02         2.93104e+02         2.91626e+02         2.88773e+02         2.81249e+02         2.90511e+02
        165         2.89726e+02         3.01021e+02         2.84675e+02         2.94252e+02         2.90555e+02         2.86262e+02         2.93265e+02         2.91763e+02         2.88898e+02         2.81403e+02         2.90629e+02
