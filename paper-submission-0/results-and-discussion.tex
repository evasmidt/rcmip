RCMs agree moderately well with historical observations of global-mean surface air temperature (GSAT) (Figure \ref{fig:historical-obs}).
For the period 2000-2019, the RCMs project warming of 0.94\unit{\degree C} with a standard deviation of 0.09\unit{\degree C} relative to a reference period of 1850-1900 and a warming rate of 0.24\unit{\degree C} / decade with a standard deviation of 0.04\unit{\degree C} / decade.
These projections agree with best-guess warming observations of 0.99\unit{\degree C} but no RCM agrees with the observed warming rate of 0.19\unit{\degree C} / decade.
With the exception of the GREB model (which is a \chem{CO_2}-only model), all the RCMs demonstrate an ability to reproduce short-term cooling due to major volcanic eruptions.

The RCMs do not exhibit the same high-frequency modes as observations due to their lack of internal variability, particularly representations of phenomena like El Nino, the Interdecadal Pacific Oscillation or Atlantic Meridional Variability.
This lack of natural variability may explain why RCMs (with the exception of GREB which is a \chem{CO_2}-only model) appear to be too cool through the 1960’s, 70’s and 80’s and then warm too quickly thereafter.
Alternately, it could be that RCMs overestimate aerosol cooling over this period (although the spread in aerosol effective radiative forcing estimates across models is nonetheless fairly large, see Supplementary Figure \ref{fig:historical-effrf}) or overestimate of the impact of the eruption of Mt Agung in 1963.

This suggests a clear area for further evaluation of RCMs and has important implications for remaining carbon budget estimates, which are highly sensitive to estimates of recent warming trends \citep{leach_2018_slci14}.
Discrepancies in the carbon cycle response to emissions also impact remaining carbon budget estimates, and further analysis on the all-greenhouse gas emissions driven runs would highlight further model differences.

The discrepancies between RCMs are of a similar order of magnitude to observational uncertainties \citep{Morice_2012_q4f}.
The spread of RCMs is also much smaller than the spread in available CMIP6 model results (warming of $1.13 \pm 0.3$\unit{\degree C} and warming rate of $0.24 \pm 0.08$\unit{\degree C} / decade when only the first available ensemble member from each model group is used).
Given their simple, tuneable nature, it is no surprise that RCMs tend to agree more closely with observations than CMIP models and exhibit less spread.

In general, RCMs can emulate CMIP6 surface air temperature change relatively well (Figure \ref{fig:emulation-comparison-example}).
Given that RCMs do not include internal variability, the lower bound for root-mean square error (RMSE) is of the same order of magnitude as internal variability in CMIP6 models.
The best emulators are pushing this limit, with RMSE on the order of 0.2K (Table \ref{tab:rcmip-emulation-scores}).
As CMIP6 results have only recently become available, we expect further calibration efforts to reduce RMSE even further.

Despite their relatively good performance, results for the different emulation setups have generally only been submitted for a limited set of scenarios (see Supplementary Table \ref{tab:emulation-scores-full} and Supplementary Figures \ref{fig:emulation-comparison-example-CanESM5-r1i1p2f1} - \ref{fig:emulation-comparison-example-NorCPM1-r1i1p1f1}).
Hence it is still not clear whether the good performance in idealised scenarios also carries over to projections, particularly for the SSPs.
Having said this, results for MAGICC7.1.0, which has supplied projections for the SSPs for each emulation setup, are promising.
MAGICC7.1.0’s results suggest that RCMs should be close to the lower limit of RMSE as more CMIP6 results become available and further calibration efforts are carried out.

From the available results, differences emerge between models with constant effective climate sensitivities and models with time or state dependent effective climate sensitivities.
Models with constant effective climate sensitivities, such as the AR5IR implementations, struggle to capture the non-linear response of ESMs to abrupt changes in \chem{CO_2} concentrations.
Firstly, they predict an equally large response to negative radiative forcing as positive radiative forcing which isn’t always the case in ESMs (Panels (d) and (e) of Figure \ref{fig:emulation-comparison-example}).
Secondly, in order to capture the long-term warming seen in many abrupt-4xCO2 experiments, one of the boxes often has a response timescale on the order of thousands of years.
This is problematic because it leads to equilibration times on the order of thousands of years and large equilibrium responses in the abrupt-2xCO2 experiments (i.e. large equilibrium climate sensitivities, see Supplementary Table \ref{tab:emulation-scores-full}).
Models with time or state dependent effective climate sensitivities avoid both those problems.
In particular, their temperature response to positive and negative radiative forcing need not be of equal magnitude and they can exhibit the long warming tail seen in ESM abrupt-4xCO2 runs whilst avoiding extremely long equilibration times and large equilibrium temperature perturbations in abrupt-2xCO2 runs.

Probabilistic projections from RCMs illustrate the large range of plausible temperature projections resulting from physical parameter sets which are consistent with observations (Figure \ref{fig:probabilistic-projections}).
For example, under the very ambitious mitigation scenario ssp119, the models presented here have a best estimate of approximately 1.5\unit{\degree C} for end of century warming.
However, they also suggest that there is still approximately a 1 in 6 chance that warming would exceed 2\unit{\degree C}.

These probabilistic projections extend the results of CMIP6, which do not include such large perturbed parameter ensemble plus constraining exercises.
The 66\% ranges presented here are, in general, significantly narrower than the CMIP6 intermodel spread.
There is no requirement that CMIP6 results lie within some range of historically observed temperature changes but the difference suggests that some caution should be used when inferring projection uncertainty from CMIP6 results alone.

Four of the models (MCE, WASP, FaIR and OSCAR) provide remarkably similar median projections.
On the other hand, Hector projects significantly smaller surface air temperature increases, likely due to its lower \chem{CO_2} radiative forcing estimates (Figure \ref{fig:probabilistic-co2-effrf}).

On the other hand, there is a surprising amount of variation in probabilistic simulations of the historical period.
The variation in ranges, from MCE with relatively large ranges, to WASP and Hector with much smaller ranges, likely reflects differences in constraining techniques.
Given that variations in both model structure and calibration technique influence probabilistic projections, an area for future research could be to try and disentangle the impact of these two components.
Such an experiment could involve constraining models with the same constraining technique or constraining a single model with two different techniques.

One other area for future research is the impact of the reference period.
Within the reference period, all model results and observations will be artificially brought together, narrowing uncertainty and disagreement within this period \citep{Hawkins_2016_f8x9vb}.
This can alter conclusions as the reference period will become less important for any fitting algorithm (because of the artificial agreement), placing more weight on other periods.
Developing a method to rebase both the mean and variance of model and observational results onto other reference periods would allow the impact of the reference period choice to be explored in a more systematic fashion.

Looking forward, it is clear that making probabilistic projections consistent with CMIP6 results requires structural model flexibility, such that models are shown to be able to reproduce CMIP6 results.
Having achieved this, probabilistic parameter ensembles can then be derived while considering uncertainty in both \chem{CO_2} and non-\chem{CO_2} climate drivers.
During RCMIP Phase 1, only Hector has been able to perform both these steps.
However, we hope that more models will be able to perform these steps in further phases.
Such results would enhance our understanding of the uncertainty in observationally consistent climate change projections and hence be of interest to the climate research community and beyond.

When run with the same model, warming projections are higher in the SSPs than the RCPs (Figure \ref{fig:rcp-ssp-comparison}).
Whilst historical warming estimates are very similar, if not slightly higher in the RCP-compatible historical runs, the scenarios separate over the course of the 21\textsuperscript{st} Century.

For the RCMIP results, we can see that the increase in warming projections is due to the higher effective radiative forcing in the SSPs throughout the second half of the 21st Century (Supplementary Figure \ref{fig:rcp-ssp-comparison-effrf}).
The higher effective radiative forcing results appears to be a result of the SSPs agreeing more closely with their nameplate 2100 radiative forcing level than the RCPs, which were generally too low.
The increased forcing is driven largely by increased \chem{CO_2} effective radiative forcing (Supplementary Figure \ref{fig:rcp-ssp-comparison-co2-effrf}), which itself is driven by increased \chem{CO_2} emissions \citep{Gidden_2019_gf8sgm}.
Even though aerosol effective radiative forcing is also slightly more negative in the SSPs (Supplementary Figure \ref{fig:rcp-ssp-comparison-aerosols-effrf}), the difference of approximately 0.1 \unit{W m^{-2}} is not enough to offset increased \chem{CO_2} effective radiative forcing of approximately 0.5 \unit{W m^{-2}}.

At present, effective radiative forcings diagnosed from the CMIP6 models are not available (as such diagnosis is not a trivial task).
However, given the monotonic relationship between \chem{CO_2} concentrations and effective radiative forcing \citep{IPCC_2013_WGI_Ch_8}, it is likely that the same mechanisms are driving at least part of the increase between CMIP5 and CMIP6 projections.

Comparing warming projections between CMIP5 and CMIP6, our results suggest that around 46\% of the increase is scenario driven.
However, this still leaves 54\% which is not explained by the change in scenarios.
At this stage, this residual is most likely explained by a change in the models submitting results to CMIP, which appear to be more sensitive to changes in atmospheric GHG concentrations in CMIP6 than in CMIP5 \citep{wyser_2019_scojd2,voldoire_2019_98cjk3,voosen_2019_9sc8df}.
However, CMIP6 analysis is ongoing and should be considered before making strong conclusions about the robustness of these findings.

As discussed previously, the results from RCMIP can provide much more information than has been presented here.
A number of experiments have not been discussed here which would shed light on the differences between the RCMs in a number of other components.
In addition, RCMs also offer the chance to explore more experiments than is planned in CMIP6 due to their computational efficiency.
An experiment which is an example of both these points is the ssp370-lowNTCF scenario as quantified by \citet{Gidden_2019_gf8sgm}, which includes reductions in methane emissions.
In contrast, the ssp370-lowNTCF as defined by AerChemMIP explicitly includes methane emissions reductions.
RCMs can examine the impact of this difference by running an extra experiment, `ssp370-lowNTCF-gidden', which follows the emissions quantified by \citet{Gidden_2019_gf8sgm}.
Preliminary results are given in Supplementary Figure \ref{fig:ssp370-lowntcf-overview} and, unsurprisingly, show that surface air temperatures rise in ssp370-lowNTCF (relative to ssp370) whilst they fall in ssp370-lowNTCF-gidden.
This fall in temperatures is driven entirely by reductions in methane emissions and users of CMIP6 data should be careful not to confuse the results of the ssp370-lowNTCF scenario with the emissions scenarios presented in \citet{Gidden_2019_gf8sgm}.
They are two different experiments.
