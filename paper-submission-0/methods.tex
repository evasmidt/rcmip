\subsection{Experimental design} % (fold)
\label{sub:experimental_design}

Phase 1 of RCMIP focuses on a limited set of the CMIP6 experiment protocol plus a few experiments from CMIP5.
On top of the experiments from CMIP6 and CMIP5 we also add other experiments which are of interest to the community (a full list is available in Supplementary Table \ref{tab:rcmip-experiment-overview}).
The first class of these are the `esm-X-allGHG’ experiments.
These runs are driven by emissions of all greenhouse gases (\chem{CO_2}, \chem{CH_4}, HFCs, PFCs etc.), rather than only \chem{CO_2} as is typical for ESMs in CMIP6.
These experiments are particularly useful to the WGIII community as they perform climate assessment based on emissions scenarios, hence need models which can run from emissions and do not require exogenous concentrations of greenhouse gases.

We also add one extra experiment, ssp370-lowNTCF-gidden, onto the ssp370-lowNTCF experiment from AerChemMIP.
The ssp370-lowNTCF experiment explicitly excludes a reduction in methane concentrations.
However, the ssp370-lowNTCF emissions dataset as described in \citet{Gidden_2019_gf8sgm} and calculated in \citet{Meinshausen_2019_gf86j8} does include reduced methane emissions and hence atmospheric concentrations.
We include a `ssp370-lowNTCF-gidden' scenario to complement ssp370-lowNTCF and examine the consequences of a strong reduction in methane emissions.

The standard set of inputs from CMIP5 and CMIP6 was translated into the RCMIP experiment protocol, using the WGIII format \citep{Gidden_2019_ggfx68}, so it could be used by the modelling groups.

CMIP6 emissions projections follow \citet{Gidden_2019_gf8sgm} and are taken from \url{https://tntcat.iiasa.ac.at/SspDb/dsd?Action=htmlpage&page=60}, hosted by IIASA.
Where WMGHG gas emissions are missing, we use inverse emissions based on the CMIP6 concentrations from MAGICC7.0.0 \citep{Meinshausen_2019_gf86j8}.
Where regional emissions information is missing, we use the downscaling procedure described in \citet{Meinshausen_2019_gf86j8}.
The emissions extensions also follow the convention described in \citet{Meinshausen_2019_gf86j8}.

For CMIP6 historical emissions, we have used data sources which match the harmonisation used for the CMIP6 emissions projections.
This ensures consistency with CMIP6, albeit at the expense of using the latest data sources in some cases.
CMIP6 historical anthropogenic emissions for \chem{CO_2}, \chem{CH_4}, BC, CO, \chem{NH_3}, NOx, OC, \chem{SO_2} and volatile organic compounds (VOCs) come from CEDS \citep{Hoesly_2018_gcxtgm}.
Biomass burning emissions data for \chem{CH_4}, BC, CO, \chem{NH_3}, NOx, OC, \chem{SO_2} and volatile organic compounds (VOCs) come from UVA \citep{Marle_2017_gbxdd4}.
The biomass burning emissions are a blend of both anthropogenic and natural emissions, which could lead to some inconsistency between RCMs as they make different assumptions about the particular anthropogenic/natural emissions split.
\chem{CO_2} global land-use emissions are taken from the Global Carbon Budget 2016 \citep{Quere_2016_bs3k}.
Other CMIP6 historical emissions come from PRIMAP-hist \citep{Guetschow_2016_f9cwhz} Version 1.0 \url{https://doi.org/10.5880/PIK.2016.003} (\chem{N_2O} and \chem{CO_2} land-use regional information).
Where required, historical emissions were extended back to 1750 by assuming a constant relative rate of decline based on the period 1850-1860.
While this means that our historical emissions are highly uncertain, all we require is consistent emissions inputs so we leave improved quantifications of emissions in this period for other research.

CMIP6 concentrations follow \citet{Meinshausen_2019_gf86j8}.
CMIP6 radiative forcings follow the data provided at \url{https://doi.org/10.5281/zenodo.3515339)}.
CMIP5 emissions, concentrations and radiative forcings follow \citet{Meinshausen_2011_fn3pw6} and are taken from \url{http://www.pik-potsdam.de/~mmalte/rcps/}.

% subsection experimental_design (end)

\subsection{Diagnostics} % (fold)
\label{sub:diagnostics}

Given their focus on global-mean, annual mean variables we request a range of output variables from each RCM (detailed in Supplementary Table \ref{tab:rcmip-variable-overview}).
The output variables focus on the response of the climate system to radiative forcing (e.g. surface air temperature change, surface ocean temperature change, effective radiative forcing, effective climate sensitivity) as well as the carbon cycle (carbon pool sizes, fluxes between the pools, fluxes due to Earth System feedbacks).
The temporal resolution of the models means that we request the data on an annual timestep but may increase the temporal resolution in Phase 2.

The output dataset represents a huge data resource.
In this paper we focus on a limited set of variables, particularly related to the climate response to radiative forcing.
The dataset extends well beyond this limited scope and should be investigated in further research.
To facilitate such research, we have put the entire database under a Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) license.

% subsection diagnostics (end)
