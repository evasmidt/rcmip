.DEFAULT_GOAL := help

ifndef CONDA_PREFIX
$(error Conda not active, please install conda and then create an environment with `conda create --name rcmip` and activate it with `conda activate rcmip`))
else
ifeq ($(CONDA_DEFAULT_ENV),base)
$(error Do not install to conda base environment. Source a different conda environment e.g. `conda activate rcmip` or `conda create --name rcmip` and rerun make with the `-B` flag))
endif
endif

PYTHON=$(CONDA_PREFIX)/bin/python
CONDA_ENV_FILE=environment.yml


SCRIPTS_DIR=scripts
DATA_DIR=data
OUTPUT_DIR=output
NOTEBOOKS_DIR=notebooks
FIGURES_DIR=figures
SECTIONS_DIR=sections
# For different papers, can either make new repositories or make sub-directories in
# the relevant top-level directories e.g. `figures/rcmip-protocol-paper`, and then
# have a `scons` directory too, perhaps.


FILES_TO_FORMAT_PYTHON=setup.py $(SCRIPTS_DIR) src tests


define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([\$$\(\)a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT


help:  ## print short description of each target
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)
	@echo ""
	@make variables

paper-final/rcmip-phase-1.pdf: $(wildcard paper-final/*.tex) paper/bibliography.bib  # make final, accepted paper
	cd paper-final; latexmk -pdf main.tex

paper/rcmip-overview.pdf: $(wildcard paper/*.tex) paper/bibliography.bib
	cd paper; latexmk -pdf rcmip-overview.tex

paper/rcmip-phase-1.pdf: $(wildcard paper/*.tex) paper/bibliography.bib
	cd paper; latexmk -pdf rcmip-phase-1.tex

# this doesn't really work, have to remove bibliography items from diff first
# before using pdflatex because of error I can't find
diff-rcmip-phase-1.pdf: paper/rcmip-phase-1.pdf  ## make pdf which shows difference between submitted and revised round 1 manuscripts
	rm paper/rcmip-phase-1.pdf
	make paper/rcmip-phase-1.pdf
	latexdiff paper-submission-1/rcmip-phase-1.tex paper/rcmip-phase-1.tex --flatten > paper/diff-rcmip-phase-1.tex
	latexdiff paper-submission-1/rcmip-phase-1.bbl paper/rcmip-phase-1.bbl > paper/diff-rcmip-phase-1.bbl
	cd paper; pdflatex diff-rcmip-phase-1.tex;mv $@ ../
	cd paper; pdflatex diff-rcmip-phase-1.tex;mv $@ ../

.PHONY: clean-paper
clean-paper:
	rm -f paper/rcmip-phase-1.pdf paper/*.log paper/*.bbl paper/*.aux paper/*.blg

.PHONY: conda-environment
conda-environment: $(CONDA_PREFIX)  ## create conda environment for analysis
$(CONDA_PREFIX): $(CONDA_ENV_FILE)
	$(CONDA_EXE) env update --name $(CONDA_DEFAULT_ENV) -f $(CONDA_ENV_FILE)
	touch $(CONDA_PREFIX)

.PHONY: clean
clean:  ## clean the repository of all artefacts
	make clean-conda-environment

.PHONY: clean-conda-environment
clean-conda-environment:  ## remove the conda environment
	rm -rf $(CONDA_PREFIX)

test: $(CONDA_PREFIX)  ## run the full testsuite
	$(CONDA_PREFIX)/bin/pytest --cov -r a --cov-report term-missing

.PHONY: test-notebooks
test-notebooks: $(CONDA_PREFIX)
	export NOTEBOOKS_DIR=$(NOTEBOOKS_DIR); \
	export CI=True; \
	scons --tree=prune,status

.PHONY: format
format:  ## re-format files
	make isort
	make black

.PHONY: black
black: $(CONDA_PREFIX)  ## auto-format the Python scripts
	@status=$$(git status --porcelain $(FILES_TO_FORMAT_PYTHON)); \
	if test ${FORCE} || test "x$${status}" = x; then \
		$(CONDA_PREFIX)/bin/black --exclude _version.py --target-version py35 $(FILES_TO_FORMAT_PYTHON); \
	else \
		echo Not trying any formatting. Working directory is dirty ... >&2; \
	fi

.PHONY: flake8
flake8: $(CONDA_PREFIX)  ## check compliance with pep8
	$(CONDA_PREFIX)/bin/flake8 $(FILES_TO_FORMAT_PYTHON)

.PHONY: isort
isort: $(CONDA_PREFIX)  ## format the imports in the source and tests
	$(CONDA_PREFIX)/bin/isort -y --recursive $(FILES_TO_FORMAT_PYTHON)


.PHONY: variables
variables:  ## display the value of all variables in the Makefile
	@echo CONDA_PREFIX: $(CONDA_PREFIX)
	@echo PYTHON: $(PYTHON)
	@echo CONDA_DEFAULT_ENV: $(CONDA_DEFAULT_ENV)
	@echo CONDA_PREFIX: $(CONDA_PREFIX)
	@echo CONDA_ENV_FILE: $(CONDA_ENV_FILE)
	@echo ""
	@echo DATA_DIR: $(DATA_DIR)
	@echo SCRIPTS_DIR: $(SCRIPTS_DIR)
	@echo NOTEBOOKS_DIR: $(NOTEBOOKS_DIR)
	@echo FIGURES_DIR: $(FIGURES_DIR)
	@echo OUTPUT_DIR: $(OUTPUT_DIR)
	@echo SECTIONS_DIR: $(SECTIONS_DIR)
	@echo ""
	@echo FILES_TO_FORMAT_PYTHON: $(FILES_TO_FORMAT_PYTHON)
