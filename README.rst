RCMIP Phase 1
=============

.. sec-begin-links

+---------+----------+
| Archive | |Zenodo| |
+---------+----------+

+--------------+-----------+
| Code license | |License| |
+--------------+-----------+

.. |License| image:: https://img.shields.io/github/license/lewisjared/scmdata.svg
    :target: https://gitlab.com/rcmip/rcmip/blob/master/LICENSE
.. |Zenodo| image:: https://zenodo.org/badge/DOI/10.5281/zenodo.4016613.svg
   :target: https://doi.org/10.5281/zenodo.4016613

.. sec-begin-index

This repository contains code and data related to Phase 1 of RCMIP.
If this repository is of use to you, please cite

    Nicholls, Z. R. J., Meinshausen, M., Lewis, J., Gieseke, R., Dommenget, D., Dorheim, K., Fan, C.-S., Fuglestvedt, J. S., Gasser, T., Golüke, U., Goodwin, P., Kriegler, E., Leach, N. J., Marchegiani, D., Quilcaille, Y., Samset, B. H., Sandstad, M., Shiklomanov, A. N., Skeie, R. B., Smith, C. J., Tanaka, K., Tsutsui, J., and Xie, Z.: Reduced complexity model intercomparison project phase 1: Protocol, results and initial observations, Geosci. Model Dev. Discuss., https://doi.org/10.5194/gmd-2019-375, in review, 2020.

The accepted manuscript is available at https://doi.org/10.5194/gmd-2019-375.
All code and data has also been archived at Zenodo (https://doi.org/10.5281/zenodo.4016613).

We are in the process of tidying up this repository.
If you have any urgent issues, please raise them in the `issue tracker <https://gitlab.com/rcmip/rcmip/issues>`_.

.. sec-end-index

Contributing
------------

If you'd like to contribute, please make a pull request!
The pull request templates should ensure that you provide all the necessary information.

.. sec-begin-license

License
-------

RCMIP is free software under a BSD 3-Clause License, see `LICENSE <https://gitlab.com/rcmip/rcmip/license/blob/master/LICENSE>`_.

Data
----

All our data is available via `Zenodo <https://doi.org/10.5281/zenodo.4016613>`_.

.. sec-end-license
