# -*- coding: utf-8 -*-

"""Console script for rcmip."""
import sys

import click


@click.group(name="rcmip")
def cli():
    return 0


@cli.command()
def validate(args=None):
    click.echo("validate")
    return 0


@cli.command()
def upload(args=None):
    click.echo("upload")
    return 0


if __name__ == "__main__":
    sys.exit(cli())  # pragma: no cover
