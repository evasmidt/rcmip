\section{Data formats} % (fold)
\label{sub:data_formats}

Systematic intercomparison projects such as RCMIP require the definition of a clear input and output data handling framework.
Historically, comparing RCMs required learning how to set up, configure and run multiple RCMs in order to produce results.
This required significant time and hence, as previously discussed, has only been attempted in standalone cases with a limited number of models \citep{houghton_1997_adf9ss,Vuuren_2009_bcmqjh,Harmsen_2015_f72ghq,Schwarber_2019_ggfp6r}.
With a common framework, once a model has participated in RCMIP, it is simpler to run it again in different experiments and provide output in a common, standardised format.
This allows researchers to design, run and analyse experiments with far less effort than was previously required.
As a result, it becomes feasible to do more regular and targeted assessment of RCMs.
This capacity improves our knowledge of RCMs, our understanding of the implications of their quantitative results and our ability to develop and improve them.
Our data format is designed to be easy to use and hence easily able to be extended within future RCMIP phases or in separate research.

\subsection{Inputs} % (fold)
\label{ssub:inputs}

All input data is provided in a text-based format based on the specifications used by the IAMC community \citep{Gidden_2019_ggfx68}.
The computational simplicity of RCMs means that their input specifications are relatively lightweight and hence using an uncompressed, text-based input format is possible.
Further, the format is explicit about associated metadata and ensures metadata remains attached to the timeseries.
As the IAMC community is a major user of RCMs, as well as being the source of input data for many experiments run with RCMs, using their data format ensures that data can be shared easily and assessment of IAM emissions scenarios can be performed with minimal data handling overhead.

The inputs are formatted as text files with comma separated values (CSV), with each row of the CSV file being a timeseries (see \url{rcmip.org}).
This format is also often referred to as `wide' although this term is imprecise \citep{wickham_tidy_data_2014}.
The columns provide metadata about the timeseries, specifically the timeseries' variable, units, region, model and scenario.
Other columns provide the values for each timestep within the timeseries.

Being simplified models, RCMs typically do not take gridded input.
Hence we use a selection of highly aggregated socio-economic regions, which once again follow IAMC conventions \citep{Gidden_2019_ggfx68}.

% subsection inputs (end)

\subsection{Outputs} % (fold)
\label{ssub:outputs}

RCMIP Phase 1's submission template (see \url{rcmip.org} or \url{https://doi.org/10.5281/zenodo.3593570}) is composed of two parts.
The first part is the data submission and is identical to the input format.
Using a consistent data format allows for simplified analysis with the same tools we used to develop the input protocols and exchange with the IAMC community as they can analyse the data using existing tools such as pyam \citep{Gidden_2019_ggfx68}.
However, one complication of using the IAMC format is that the `model' column is reserved for the name of the integrated assessment model which produced the scenario.
To enhance compatibility with the IAMC format, we don't use the `model' column.
Instead, we use the separate `climate\_model' column to store metadata about the climate model which provided the timeseries.

The second part of submissions is model metadata.
This includes the model's name, version number, brief description and literature reference.
We also request a configuration label, which uniquely identifies the configuration in which the model was run to produce the given results.

Given the typical temporal resolution of RCMs, we request all output be reported with an annual timestep.
In addition, to facilitate use of the output, participating modelling groups agree to have their submitted data made available under a Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) license.
All input and output data, as well as all code required to produce this paper, is available at \url{gitlab.com/rcmip/rcmip} and archived at \url{https://doi.org/10.5281/zenodo.3593569}.

% subsection outputs (end)

% section data_formats (end)

\section{Scenario-based experiments data sources} % (fold)
\label{sub:scenario_based_experiments_data_sources}

CMIP6 emissions projections follow \citet{Gidden_2019_gf8sgm} and are available at \url{https://tntcat.iiasa.ac.at/SspDb/dsd?Action=htmlpage&page=60} (hosted by IIASA).
Where regional emissions information is missing, we use the downscaling procedure described in \citet{Meinshausen_2019_gf86j8}.
The emissions extensions also follow the convention described in \citet{Meinshausen_2019_gf86j8}.

For CMIP6 historical emissions (year 1850-2014), we have used data sources which match the harmonisation used for the CMIP6 emissions projections.
This ensures consistency with CMIP6, although it means that we do not always use the latest data sources.
CMIP6 historical anthropogenic emissions for \chem{CO_2}, \chem{CH_4}, BC, CO, \chem{NH_3}, NOx, OC, \chem{SO_2} and non-methane volatile organic compounds (NMVOCs) come from CEDS \citep{Hoesly_2018_gcxtgm}.
Biomass burning emissions data for \chem{CH_4}, BC, CO, \chem{NH_3}, NOx, OC, \chem{SO_2} and NMVOCs come from UVA \citep{Marle_2017_gbxdd4}.
The biomass burning emissions are a blend of both anthropogenic and natural emissions, which could lead to some inconsistency between RCMs as they make different assumptions about the particular anthropogenic/natural emissions split.
\chem{CO_2} global land-use emissions are taken from the Global Carbon Budget 2016 \citep{Quere_2016_bs3k}.
Emissions of \chem{N_2O} and the regional breakdown of \chem{CO_2} land-use emissions come from PRIMAP-hist Version 1.0 \citep[][see \url{https://doi.org/10.5880/PIK.2016.003}]{Guetschow_2016_f9cwhz}.
Where well-mixed greenhouse gas emissions are missing, we use inverse emissions based on the CMIP6 concentrations from MAGICC7.0.0 \citep{Meinshausen_2019_gf86j8}.
Where required, historical emissions were extended back to 1750 by assuming a constant relative rate of decline based on the period 1850-1860 (noting that historical emissions are somewhat uncertain, we require consistent emissions inputs in Phase 1, uncertainty in historical emissions will be explored in future research).

CMIP6 concentrations follow \citet{Meinshausen_2019_gf86j8}.
CMIP6 radiative forcings follow the data provided at \url{https://doi.org/10.5281/zenodo.3515339)}.
CMIP5 emissions, concentrations and radiative forcings follow \citet{Meinshausen_2011_fn3pw6} and are taken from \url{http://www.pik-potsdam.de/~mmalte/rcps/}.

% section scenario_based_experiments_data_sources (end)

\clearpage

\input{emulation-scores-table-tidy}

\clearpage

\input{emulation-si-figures}

% \clearpage

% \input{region-summary-table}

% \clearpage

% \input{experiment-summary-table}

\clearpage

\input{variable-summary-table}

\clearpage

