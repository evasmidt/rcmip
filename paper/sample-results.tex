Within three months of beginning RCMIP and publishing the protocols, 15 different RCMs submitted data.
Given that this is the first phase of RCMIP, we expect even shorter turnarounds in future.
The submitted results demonstrate that the RCM community, via RCMIP, now has the capacity to run multi-model studies, and to run them comparatively quickly.
In addition, the number of participating modelling groups demonstrates that the RCMIP infrastructure is accessible to a wide range of modelling teams.

All the RCMs are able to capture the approximately 1\degree C of warming seen in the historical observations (Figure \ref{fig:historical-obs}), compared to a pre-industrial reference period \citep{Richardson_2016_f9cgff, Rogelj_2019_gf45fs}.
However, the RCMs vary in the detail which they represent.
Most of the RCMs include some representation of the impact of volcanic eruptions, most notably the drop in global-mean temperatures after the eruption of Mount Agung in 1963.
In addition, most of the RCMs do not capture natural variability driven by processes such as the El Ni\~{n}o Southern Oscillation \citep{wolter_enso_2011}, the Pacific Decadal Oscillation \citep{zhang_pdo_1997} and the Indian Ocean Dipole \citep{Saji_iod_1999}.
The exception to this is the EMGC model, which includes representations of the impact of all of these processes.
At the other end of the complexity spectrum, we have the \chem{CO_2}-only model, GREB.
Unlike the other RCMs, GREB lacks the volcanic and aerosol induced cooling signals of the 19\textsuperscript{th} and 20\textsuperscript{th} Centuries.

RCMIP also facilitates a comparison of model calibrations and CMIP output (Figure \ref{fig:emulation-comparison-example}).
Examining multiple emulation setups, we see that RCMs can reproduce the temperature response of CMIP models to forcing changes to within a root-mean square error of 0.2\degree C (Table \ref{tab:rcmip-emulation-scores}).
A detailed comparison of RCMs with 24 CMIP6 ESM ensemble members is available in the Supplementary (Table \ref{tab:emulation-scores-full} and Supplementary Figures \ref{fig:emulation-comparison-example-CanESM5-r1i1p2f1} to \ref{fig:emulation-comparison-example-CNRM-CM6-1-r1i1p1f2}).
In scenario-based experiments, it appears to be harder for RCMs to emulate CMIP output than in idealised experiments.
We suggest two key explanations.
The first is that effective radiative forcing cannot be easily diagnosed in SSP-based scenarios hence it is hard to know how best to force the RCM during calibration.
The second is that the forcing in these scenarios includes periods of increase, sudden decrease due to volcanoes as well as longer term stabilisation rather than the simpler changes seen in the idealised experiments.
Fitting all three of these regimes is a more difficult challenge than fitting the idealised experiments alone.

Only 6 models (Table \ref{tab:rcmip-emulation-scores}) have been able to submit emulation configurations.
Furthermore, each RCM is calibrated to a different number of CMIP models, with some modeling teams unable to provide any calibrations at all.
The reason is that there is to-date no common resource of calibration data from the CMIP6 repositories.
The technical challenge of diagnosing, stitching together, creating area-weighted averages and de-drifting a large amount of CMIP6 output data within a short time period has turned out to be a hurdle for many modelling teams.
As an off-spring from RCMIP, we attempt to address this challenge for the future by providing a unifying data portal \citep[see \url{cmip6.science.unimelb.edu.au},][]{nicholls_gdj2020_cmipaggregate}.

The ensemble of RCMs also provides insights into the differences between CMIP5 and CMIP6 generation scenarios (`RCP' and `SSP-based' scenarios respectively) when these scenarios are run with identical models (Figure \ref{fig:rcp-ssp-comparison}).
In the selection of models which have submitted all RCP, SSP-based scenario pairs, the SSP-based scenarios are 0.20\degree C (standard deviation 0.10\degree C across the available models) warmer than their corresponding RCPs (Figure \ref{fig:rcp-ssp-comparison}(b)).
This difference is driven by the 0.39 $\pm 0.24$ \unit{W m^{-2}} larger effective radiative forcing in the SSP-based scenarios (Figure \ref{fig:rcp-ssp-comparison}(d)), which itself is driven by the 0.53 $\pm 0.44$ \unit{W m^{-2}} larger \chem{CO_2} effective radiative forcing in the SSP-based scenarios (Figure \ref{fig:rcp-ssp-comparison}(f)).
These results add to the work of \citet{Wyser_2020} which suggests that even when run with the same model (in a concentration-driven setup), the SSP-based scenarios result in warmer projections than the RCPs.
When we run one of the RCMs (MAGICC) with an AR5-consistent stratospheric-adjusted radiative forcing definition \citep{IPCC_2013_WGI_Ch_8}, the SSP-based and RCP scenarios are within 6\% of each other in 2100 (albeit their AR5-consistent stratospheric-adjusted radiative forcing trajectories can differ by up to 15\% at different times over the 21\textsuperscript{st} Century).
Thus, we find that the update to effective radiative forcing \citep{forster_2016_erfdiagnosis}, mainly using the formulations presented in \citet{Etminan_2016_gdkwbj} plus any rapid adjustment terms \citep{smith_2018_pksc99}, increases the total forcing in the SSP-based scenarios, because their generally higher \chem{CO_2} concentrations are partially, but not fully, offset by lower \chem{CH_4} concentrations \citep[see e.g. Fig. 11 in][]{Meinshausen_2019_gf86j8}.
There is a clear need for further, more comprehensive exploration of the differences between the RCP and SSP-based scenarios.

Finally, we present variations in the relationship between surface air temperature change and cumulative \chem{CO_2} emissions from the 1pctCO2 and 1pctCO2-4xext experiments (Figure \ref{fig:tcre-plot}).
To date, only three models (GIR, MCE and OSCAR) have been able to provide the required outputs (in particular deriving inverse emissions from these concentration-defined experiments).
From the available results, it is clear that the relationship between these two key variables varies over MCE's parameter ensemble, from weakly sub-linear to weakly super-linear.
Such variation can have notable implications for the remaining carbon budget \citep{nicholls_carbon_budget_implications_2020}.
We also see that the MCE model’s parameter ensemble covers a large range, dwarfing the differences between it and the GIR and OSCAR models, which are shown here in their 3\degree C climate sensitivity configurations.
This suggests that, at least for RCMs, the response of individual components and their configuration is more important than model structure, although this conclusion is tempered by the paucity of available results.
