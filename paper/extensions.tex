RCMIP Phase 1 provides proof of concept of the RCMIP approach to RCM evaluation, comparison and examination.
However, Phase 1 has been limited to a very specific set of questions and there is wide scope to use RCMs to examine other scientific questions of interest.
In this section we present a number of ways in which further research and phases of RCMIP could build on the work presented in this paper.

The first is an exploration of probabilistic outputs.
Most RCMs can be calibrated, i.e. have their parameters adjusted, such that they reproduce our best-estimate (typically median) observations.
However, RCMs are also used in a probabilistic mode.
In this mode a parametric ensemble is run for a given RCM and set of climate forcers.
The results are then used to capture the likelihood that different climate changes will unfold, particularly the likelihood of reaching different warming levels.
Given the widespread use of probabilistic distributions, particularly for quantifying likely ranges of climate sensitivity and climate projections \citep[see e.g.][]{Meinshausen_2009_b9j8fj,Skeie_2018_gdstvt,Vega-Westhoff_2019_ggftgq}, examining the differences between existing probabilistic model setups is an obvious next step.

Secondly, there is a wide range of RCMs available in the literature.
This variety can be confusing, especially to those who are not intimately involved in developing the models.
An overview of the different models, their structure and relationship to one another (in the form of a genealogy) would help reduce the confusion and provide clarity about the implications of using one model over another.

Thirdly, emulation results have generally only been submitted for a limited set of experiments.
Hence it is still not clear whether the emulation performance seen in idealised experiments also carries over to scenarios, particularly the SSP-based scenarios.
As the number of available CMIP6 results continues to grow, this area is ripe for investigation and will lead to improved understanding of the limits of the reduced complexity approach.
The development of a common resource \citep[see \url{cmip6.science.unimelb.edu.au},][]{nicholls_gdj2020_cmipaggregate} for RCM calibration will greatly aid this effort by ensuring that each group has access to the same set of calibration data.

Finally, while evaluating RCMs is a useful exercise, the root causes of these differences may not be clear.
This can be addressed by performing experiments which specifically diagnose the reasons for differences between models e.g. simple pulse emissions of different species or prescribed step changes in atmospheric greenhouse gas concentrations.
Such experiments could build on existing research \citep{Vuuren_2009_bcmqjh,Schwarber_2019_ggfp6r} and would allow even more comprehensive examination and understanding of RCM behaviour.
This would require custom experiments, particularly for the carbon cycle, which is strongly coupled to other parts of the climate system.
However, unlike ESMs, adding extra RCM experiments adds relatively little technical or human burden, because RCMs are computationally cheap and because RCMIP’s standardised formats facilitate highly automated experiment pipelines.
