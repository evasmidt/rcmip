In an ideal world, sufficient computing power would enable running our most comprehensive, physically complete models for every application of interest.
However, computational limits do exist so we must sometimes turn to other approaches.
One common approach is the use of reduced complexity climate models (RCMs).

RCMs typically exchange limited spatial and temporal resolution for computational efficiency.
Specifically, they usually focus on global-mean, annual-mean quantities.
As a result, they are usually on the order of a million times faster than more complex models (in terms of simulated model years per unit CPU time).

The computational efficiency of RCMs means that they can be used where computational constraints would otherwise be limiting.
Such cases include performing climate assessment for a large number of scenarios, exploring interacting uncertainties from multiple parts of the climate system or combining multiple lines of evidence in an internally consistent setup.
In the IPCC context, a prominent example is the climate assessment of the WGIII socioeconomic scenarios.
Given there are hundreds of emission scenarios submitted by Integrated Assessment Models in IPCC AR5 and AR6 (available at \url{https://secure.iiasa.ac.at/web-apps/ene/AR5DB} and \url{https://tntcat.iiasa.ac.at/SspDb}, hosted by the IIASA Energy Program) it is unfeasible to perform climate assessment with the world’s most comprehensive models.
Instead, reduced complexity models are used.

Beyond their computational efficiency, RCMs also offer conceptual simplicity. This gives them a second use: aiding in interpreting results from higher complexity models or observations. While we think this second use is also valuable (see e.g. \citet{Held_2010_bjj8vh} and \citet{Gregory_2004_bm82st}), it is beyond the scope of this paper.

When RCMs are used to overcome computational constraints, they are typically used in one of two ways.
`Emulation' mode, where the RCMs are run in a setup which has been tuned to reproduce the behaviour of a Coupled Model Intercomparison Project (CMIP) \citep{eyring_2016_fjksdl,Taylor_2012_b7xgw2} model as closely as possible over a range of scenarios. `Probabilistic' mode, where the RCMs are run with a parameter ensemble which captures the uncertainty in historical observations.
The resulting projections provide a plausible, observationally consistent range of projections which is large enough to provide useful statistics.
In some cases they are also used in a combination of the two.
For example, the MAGICC6 probabilistic setup used in the IPCC’s Fifth Assessment Report \citep{IPCC_2013_WGI_SPM} used randomly drawn emulations for the carbon cycle response whilst using a probabilistic parameter ensemble for the climate response to radiative forcing \citep{Meinshausen_2009_b9j8fj}.

The validity of both approaches rests on the premise that RCMs are able to replicate the response characteristics of the Earth System and our most complete models.
This ability is generally quantified in two different ways.
The first is a comparison with observations.
These comparisons provide the most direct comparison of model response with the world around us today.
However, given the limited amount of observations available, comparing only with observations leaves us with little understanding of how RCMs perform in scenarios apart from one in which anthropogenic emissions are causing the climate to warm.
Given our range of plausible futures, it is vital to also assess RCMs in other scenarios (e.g. reducing anthropogenic emissions, instantaneous changes in atmospheric CO2 concentrations, modifications to the solar constant).
For example, an RCM that exhibits effectively constant climate feedbacks might be able to replicate an ESM’s point response under an idealised scenario, but might not compare well to the longer-term response under either higher forcing or lower overshoot scenarios \citep{Rohrschneider_2019_ggfx33}.

Whilst the results of more comprehensive models may not represent the behaviour of the actual earth system, they are the best representation of our knowledge of the earth system's behaviour.
Comparing RCM behaviour with more complex model behaviour in these `non-historical' experiments allows us to quantify the extent to which RCMs can capture the range of responses consistent with our best understanding of the earth system.

In Phase 1 of RCMIP we benchmark current RCM performance by comparing them with each other, observations and CMIP results over a range of experiments.
This allows us to understand their strengths, weaknesses and limitations so that we can make more confident, informed conclusions from their quantitative results.
RCMIP focuses on RCMs and is not one of the CMIP6 \citep{eyring_2016_fjksdl} endorsed intercomparison projects that are designed for Earth System Models.
However, RCMIP replicates the experimental design of many of the CMIP-endorsed MIPs, particularly the DECK simulations \citep{eyring_2016_fjksdl}, ScenarioMIP \citep{ONeill_2016_f87n56}, AerChemMIP \citep{Collins_2017_gcc5z6} and others, e.g. ZECMIP \citep{jones_2019_976cds}, DAMIP \citep{Gillett_2016_f89cqb} or PMIP4 \citep{kageyama_2018_fhugcd}.

RCMIP builds on previous efforts to compare and understand RCMs.
In 1997, the IPCC Technical Paper \citep{houghton_1997_adf9ss} investigated simple climate models used in the IPCC Second Assessment Report and compared their performance against idealised AOGCM results.
\Citet{Vuuren_2009_bcmqjh} compared RCMs used in integrated assessment models (IAMs).
They focused on five \chem{CO_2}-only experiments to quantify the differences in the behaviour of each IAM’s climate component (each of which is an RCM due to computational constraints).
\citet{Harmsen_2015_f72ghq} extended the work of \citet{Vuuren_2009_bcmqjh} to consider the impact of non-\chem{CO_2} climate drivers in the RCPs.
Recently, \citet{Schwarber_2019_ggfp6r} proposed a series of impulse tests for simple climate models in order to isolate differences in model behaviour under idealised conditions.
RCMIP expands on the scope of previous work to include a broader range of scenarios and to make the first systematic comparison with both observations and CMIP model output.

\subsection{Science questions} % (fold)
\label{sub:science_questions}

Over the course of its lifetime, RCMIP intends to answer the following scientific questions.
This Phase 1 paper sets out the protocols required to do so.
However, given the scope of the questions, they cannot all be answered in a single paper and so some aspects will receive less attention here than others.

\begin{enumerate}
    \item How do existing RCMs vary in their response to changes in well-mixed greenhouse gas (WMGHG) emissions, WMGHG concentrations, anthropogenic aerosol precursor emissions and natural changes in effective radiative forcing?
    \item To what extent can RCMs emulate the response of more complex models from CMIP5 and CMIP6?
    \item How do probabilistic ensembles from RCMs compare to each other and observations and what can they tell us about projected warming uncertainty under different scenarios?
\end{enumerate}

% subsection science_questions (end)
