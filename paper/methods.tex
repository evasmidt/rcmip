\subsection{Experimental design} % (fold)
\label{sub:experimental_design}

Phase 1 of RCMIP focuses on a limited set of the CMIP6 experiment protocol plus a few experiments from CMIP5.
On top of the experiments from CMIP6 and CMIP5 we also add other experiments which are of interest to the community (a full list is available in Supplementary Table \ref{tab:rcmip-experiment-overview}).
The first class of these are the `esm-X-allGHG’ experiments.
These runs are driven by emissions of all greenhouse gases (\chem{CO_2}, \chem{CH_4}, HFCs, PFCs etc.), rather than only \chem{CO_2} as is typical for ESMs in CMIP6.
These experiments are particularly useful to the WGIII community as they perform climate assessment based on emissions scenarios, hence need models which can run from emissions and do not require exogenous concentrations of greenhouse gases.

We also add one extra experiment, ssp370-lowNTCF-gidden, onto the ssp370-lowNTCF experiment from AerChemMIP.
The ssp370-lowNTCF experiment explicitly excludes a reduction in methane concentrations.
However, the ssp370-lowNTCF emissions dataset as described in \citet{Gidden_2019_gf8sgm} and calculated in \citet{Meinshausen_2019_gf86j8} does include reduced methane emissions and hence atmospheric concentrations.
We include a `ssp370-lowNTCF-gidden' scenario to complement ssp370-lowNTCF and examine the consequences of a strong reduction in methane emissions.

The standard set of inputs from CMIP5 and CMIP6 was translated into the RCMIP experiment protocol, using the WGIII format \citep{Gidden_2019_ggfx68}, so it could be used by the modelling groups.

CMIP6 emissions projections follow \citet{Gidden_2019_gf8sgm} and are taken from \url{https://tntcat.iiasa.ac.at/SspDb/dsd?Action=htmlpage&page=60}, hosted by IIASA.
Where WMGHG gas emissions are missing, we use inverse emissions based on the CMIP6 concentrations from MAGICC7.0.0 \citep{Meinshausen_2019_gf86j8}.
Where regional emissions information is missing, we use the downscaling procedure described in \citet{Meinshausen_2019_gf86j8}.
The emissions extensions also follow the convention described in \citet{Meinshausen_2019_gf86j8}.

For CMIP6 historical emissions, we have used data sources which match the harmonisation used for the CMIP6 emissions projections.
This ensures consistency with CMIP6, albeit at the expense of using the latest data sources in some cases.
CMIP6 historical anthropogenic emissions for \chem{CO_2}, \chem{CH_4}, BC, CO, \chem{NH_3}, NOx, OC, \chem{SO_2} and volatile organic compounds (VOCs) come from CEDS \citep{Hoesly_2018_gcxtgm}.
Biomass burning emissions data for \chem{CH_4}, BC, CO, \chem{NH_3}, NOx, OC, \chem{SO_2} and volatile organic compounds (VOCs) come from UVA \citep{Marle_2017_gbxdd4}.
The biomass burning emissions are a blend of both anthropogenic and natural emissions, which could lead to some inconsistency between RCMs as they make different assumptions about the particular anthropogenic/natural emissions split.
\chem{CO_2} global land-use emissions are taken from the Global Carbon Budget 2016 \citep{Quere_2016_bs3k}.
Other CMIP6 historical emissions come from PRIMAP-hist \citep{Guetschow_2016_f9cwhz} Version 1.0 \url{https://doi.org/10.5880/PIK.2016.003} (\chem{N_2O} and \chem{CO_2} land-use regional information).
Where required, historical emissions were extended back to 1750 by assuming a constant relative rate of decline based on the period 1850-1860.
While this means that our historical emissions are highly uncertain, all we require is consistent emissions inputs so we leave improved quantifications of emissions in this period for other research.

CMIP6 concentrations follow \citet{Meinshausen_2019_gf86j8}.
CMIP6 radiative forcings follow the data provided at \url{https://doi.org/10.5281/zenodo.3515339)}.
CMIP5 emissions, concentrations and radiative forcings follow \citet{Meinshausen_2011_fn3pw6} and are taken from \url{http://www.pik-potsdam.de/~mmalte/rcps/}.

% subsection experimental_design (end)

\subsection{Diagnostics} % (fold)
\label{sub:diagnostics}

Given their focus on global-mean, annual mean variables we request a range of output variables from each RCM (detailed in Supplementary Table \ref{tab:rcmip-variable-overview}).
The output variables focus on the response of the climate system to radiative forcing (e.g. surface air temperature change, surface ocean temperature change, effective radiative forcing, effective climate sensitivity) as well as the carbon cycle (carbon pool sizes, fluxes between the pools, fluxes due to Earth System feedbacks).
The temporal resolution of the models means that we request the data on an annual timestep but may increase the temporal resolution in Phase 2.

The output dataset represents a huge data resource.
In this paper we focus on a limited set of variables, particularly related to the climate response to radiative forcing.
The dataset extends well beyond this limited scope and should be investigated in further research.
To facilitate such research, we have put the entire database under a Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) license.

% subsection diagnostics (end)

\subsection{Participating models} % (fold)
\label{sub:participating_models}

The models participating in RCMIP Phase 1 cover a broad spectrum of approaches (Table \ref{tab:rcmip-model-overview}).
In the climate response to radiative forcing, they range from two-box impulse response models to hemispherically resolved upwelling-diffusion ocean models and in the carbon cycle they range from no carbon cycle to regionally resolved, internal book-keeping modules.
Technical details of the models are summarized in Supplementary Table \ref{tab:rcmip-model-technical-overview} and described below.

\subsubsection{ACC2} % (fold)
\label{ssub:acc2}

The Aggregated Carbon Cycle, Atmospheric Chemistry, and Climate model (ACC2) \citep{Tanaka2007aggregated} is a reduced-complexity model developed from its predecessors \citep{Hooss_2001_ct8pxp,  Bruckner_2003_fr9t23}.
ACC2 describes physical and biogeochemical processes of the earth system at the global-annual-mean level and comprises three modules: i) carbon cycle, ii) atmospheric chemistry and radiative forcing, and iii) climate.
The carbon cycle module is a four-box atmosphere-ocean mixed layer model coupled with a four-box land biosphere model.
Each box is characterized by a distinct time constant, capturing carbon cycle processes operating on different time scales.
Saturating ocean \chem{CO_2} uptake under rising atmospheric \chem{CO_2} concentration is dynamically modeled by considering the thermodynamic equilibrium for ocean carbonate species.
The \chem{CO_2} fertilization effect of the land biosphere is parameterized by the beta factor and assumed to depend logarithmically on the fractional change in the atmospheric \chem{CO_2} concentration relative to preindustrial.
Carbon cycle processes are assumed to be insensitive to the change in surface temperature.
ACC2 deals with the radiative forcing of \chem{CO_2}, \chem{CH_4}, \chem{N_2O}, \chem{O_3}, \chem{SF_6}, 29 species of halocarbons, three types of aerosols, and stratospheric \chem{H_2O}.
The lifetime of \chem{CH_4} is related to the OH concentration, which further depends on the \chem{CH_4} concentration and NOx, CO, and VOC emissions.
The climate module is the Diffusion Ocean Energy Balance Climate Model (DOECLIM) \citep{Kriegler2005, Tanaka2007aggregated}, a land-ocean energy balance model coupled with a heat diffusion model describing the heat transfer to the deep ocean.

Uncertain parameters such as climate sensitivity and beta factor for \chem{CO_2} fertilization are optimized based on an inverse estimation method considering historical observations and associated uncertainties \citep{Tanaka_2009_dgwx7g}.
The model is thus designed to produce a set of `best' outcomes under different assumptions, but not probabilistic outcomes.
In RCMIP, model version 4.2 \citep{Tanaka_2018_gc8sfm} is used.

% subsubsection acc2 (end)

\subsubsection{AR5IR} % (fold)
\label{ssub:ar5ir}

The Fifth Assessment Report Impulse Response Model (AR5IR) was used for metric calculations in Chapter 8 of the IPCC’s Fifth Assessment Report \citep{IPCC_2013_WGI_Ch_8}.
Here we focus only on its temperature response to (effective) radiative forcing and do not incorporate any gas cycles.
AR5IR is an impulse response model with two timescales by default.
We also provide a three timescale version for comparison.

AR5IR is provided in a default setup which broadly reproduces historical GSAT observations with an equilibrium climate sensitivity of 3\unit{\degree C}.
AR5IR has also been calibrated to the idealised \chem{CO_2}-only experiments (abrupt-2xCO2, abrupt-4xCO2, abrupt-0p5xCO2, 1pctCO2) of a number of CMIP6 models.
Only these calibrations are reported as emulating CMIP6 models for experiments including non-\chem{CO_2} climate drivers requires model specific estimates of non-\chem{CO_2} radiative forcings, which were not available during the calibration process.
Throughout the calibrations, a default forcing due to a doubling of atmospheric \chem{CO_2} of 3.74 \unit{W m^{-2}} was chosen for simplicity.
Using CMIP6 model specific values would not change results as it would simply cause all the calibrated parameters to be rescaled by a constant factor.

% subsubsection ar5ir (end)

\subsubsection{CICERO-SCM} % (fold)
\label{ssub:cicero_scm}

The CICERO Simple climate model (CICERO-SCM) consists of an energy balance/upwelling diffusion model, a carbon cycle model \citep{Joos_1996_ggfk4m} and simplified expressions relating emissions of 38 components to forcing, either directly or via concentrations \citep{Skeie_2017_gft5nh}.
The energy balance/upwelling diffusion model \citep{Schlesinger1992ImplicationOA} calculates warming separately for the two hemispheres and includes 40 vertical layers in the ocean.
Heat is transported in the ocean by down welling of polar water and by diffusion.
A posteriori distribution of the key parameters describing the mixing processes as well as the climate sensitivity and aerosol forcing has been estimated using observations of atmospheric temperature change and ocean heat content \citep{Skeie_2018_gdstvt}.

The default model version used in this study has a climate sensitivity of 2.0\unit{\degree C} and the rest of the parameters and the aerosol forcing is set to the mean value of the ensemble members with climate sensitivity in the range of 1.95 to 2.05\unit{\degree C} from \citet{Skeie_2018_gdstvt}.
For the model version with 3.0\unit{\degree C}, the same approach is done for climate sensitivity in the range of 2.9 to 3.1\unit{\degree C}.
A detailed description of the CICERO-SCM is presented by \citet{Skeie_2017_gft5nh}.
Here, new expressions of \chem{CO_2}, \chem{CH_4} and \chem{N_2O} radiative forcing \citep{Etminan_2016_gdkwbj} are implemented in the SCM and indirect aerosol forcing is linearly scaled with \chem{SO_2} emissions rather than logarithmic as previously specified in the model.
For the emission driven simulations, the time development of the natural emissions of \chem{N_2O} and \chem{CH_4} are adjusted to match the observed historical concentration.
The mean value of the natural emissions for the last 10 years is used for the future period.

% subsubsection cicero_scm (end)

\subsubsection{ESCIMO} % (fold)
\label{ssub:escimo}

The Earth System Climate Interpretable Model \citep[ESCIMO,][]{randers_2016_sfdj8e} is a system dynamics simulation model designed to make it simple and inexpensive for policy makers to estimate the effects of various possible human interventions proposed to influence the global mean temperature in this century.
It is simple enough to run on a laptop in seconds, and to make it possible to understand what goes on in the model system.
ESCIMO is one integrated model consisting of sectors that track i) global carbon flows, ii) global energy flows, and iii) global albedo change.
ESCIMO, although simple, is capable of recreating the broad outline of the global climate history from 1850 to 2015.
ESCIMO is also able to recreate the projections of  global mean temperature GMST and other variables such as ice cover, ocean acidification, heat flow to the deep ocean, and carbon uptake in biomass, which are generated by the more complex climate models over the commonly used RCP scenarios.
One feature of ESCIMO is that it generates run-away warming if global-mean land-ocean blended surface temperature (GMST) is allowed to rise more than 1\unit{\degree C} relative to preindustrial times.
However, the ensuing warming is slow and takes hundreds of years to lift the GMST by 3\unit{\degree C}.

% subsubsection escimo (end)

\subsubsection{FaIR} % (fold)
\label{ssub:fair}

The Finite-amplitude Impulse Response (FaIR) model is an emissions-driven simple climate model written in Python.
FaIR v1.0 was originally designed to model the state-dependent carbon cycle with accumulated atmospheric carbon and temperature to \chem{CO_2} experiments \citep{Millar_2017_gbkknx} based on the impulse response relationship in IPCC AR5 \citep{IPCC_2013_WGI_Ch_8}, which is in turn built upon pulse-response experiments from Earth system models of full and intermediate complexity \citep{Joos_2013_f4td6k}.
The radiative forcing is coupled with a two time-constant global mean temperature response \citep{Geoffroy_2013_f4sjgr}.
FaIR was extended to include emissions of non-\chem{CO_2} greenhouse gases and short-lived climate forcers in v1.3 \citep{Smith_2018_gdrwm6}, reporting 13 categories of (effective) radiative forcing using simple emissions to forcing relationships available from AR5 and later \citep{Etminan_2016_gdkwbj, Stevens_2015_f7fvqc, Ghan_2013_ggfkx9, IPCC_2013_WGI_Ch_8, Myhre_2013_f4q383, Stevenson_2013_f3cvx2}.
FaIR v1.5 used in this analysis includes modules for inverting \chem{CO_2} concentrations to emissions and additional functionality required to run many of the experiments in RCMIP.

For the default case, a representative TCR of 1.7\unit{\degree C} was chosen.
Owing to tropospheric rapid adjustments \citep{smith_2018_pksc99}, an effective radiative forcing from a doubling of \chem{CO_2} of 4.01 \unit{W m^{-2}} is used, higher than the 3.71 \unit{W m^{-2}} used in \citep{IPCC_2013_WGI_Ch_8}.
Furthermore, all experiments in this paper were run using the older GHG concentrations to radiative forcing relationships from \citet{IPCC_2013_WGI_Ch_8}, firstly as the \citet{Etminan_2016_gdkwbj} relationship breaks down above 3000 ppm CO2, and secondly because it is complicated to invert concentrations of \chem{CO_2} to emissions where \chem{CO_2} overlaps with other species in the radiative transfer parameterisation.
To account for the substantial revision of methane radiative forcing in \citet{Etminan_2016_gdkwbj}, the \citet{IPCC_2013_WGI_Ch_8} values are increased by 25\%.
For the probabilistic ensemble, a 300-member sample of ECS, TCR, carbon cycle sensitivity, effective radiative forcing (for non-aerosol forcing categories) and emissions to forcing parameters for aerosols were used, with the ensemble constrained based on 1850-2015 warming from reconstructed GSAT \citep{Richardson_2016_f9cgff}.

% subsubsection fair (end)

\subsubsection{GIR} % (fold)
\label{ssub:gir}

GIR is a General Impulse Response Model built with the aim of achieving maximal simplicity, transparency and useability while remaining physically accurate enough for use in climate policy, research and teaching.
It is based on the carbon cycle of FaIR v1.0 \citep{Millar_2017_gbkknx}, but replaces a key model step -- the numerical solution of a non-linear equation -- with an analytic formula.
It then extends the carbon cycle model out to other gases through parameter selection.
The thermal response model is identical to the thermal response of both FaIR v1.0 and v1.5; though we allow the user to specify the number of thermal response timescales, rather than constraining the model to the usual two boxes.
The model is therefore 6 equations in its entirety.
The identical treatment of all greenhouse gases and aerosols (we find that aerosols can also be adequately represented through appropriate parameter selection) allows the model to take advantage of Python’s array calculation infrastructure.
It is therefore extremely rapid to run hundreds of thousands of scenario and parameter ensembles with GIR.

% subsubsection gir (end)

\subsubsection{GREB} % (fold)
\label{ssub:greb}

The Globally Resolved Energy Balance (GREB) model, also referred to as the Monash Simple Climate Model (MSCM) \citep{Dommenget_2019_ggfx32}, is a 3.75\degree x 3.75\degree global model on three vertical layers: surface, atmosphere and subsurface ocean.
It was originally developed to simulate the globally resolved surface temperature response to a change in \chem{CO_2} forcing, but is also used to study other elements of the mean climate state.
The main physical processes that control the surface temperature tendencies are simulated: solar (short-wave) and thermal (long-wave) radiation, the hydrological cycle (including evaporation, moisture transport and precipitation), horizontal transport of heat and heat uptake in the subsurface ocean.
Atmospheric circulation and cloud cover are a seasonally prescribed boundary condition, and state-independent flux corrections are used to keep the GREB model close to the observed mean climate.
Thus, GREB does not simulate the atmospheric or ocean circulation and is therefore conceptually very different from GCM simulations.

The model does simulate important climate feedbacks such as water vapour and ice-albedo feedback, but an important limitation of the GREB model is that the response to external forcings does not involve circulation or cloud feedbacks \citep{Dommenget_2011_dvqw5t}.
The latest version (GREB v1.0.1) includes a new hydrological cycle model, which represents a simple and fast tool for the study of precipitation from a large-scale climate perspective, as well as to assess its response to climate variability (e.g. El Niño or climate change) and to external forcings.
Three separate parameterisations are improved in the model: precipitation, evaporation and the circulation of water vapour \citep{Stassen_2019_ggfk4b}.
A series of GREB experiment results and more introduction can be simply accessed on the official model website at \url{http://monash.edu/research/simple-climate-model/mscm/overview_i18n.html?locale=EN}.
The last stable version code is available on GitHub at \url{https://github.com/christianstassen/greb-official/releases}.
For RCMIP scenarios, the model is run with all processes on.
The climatology used for flux correction is taken from ERA-Interim Reanalysis IFS model (ECMWF\_IFS) within a time window from 1979 to 2015.
Other parameters and input variables (not initial conditions), are taken from either ERA-Interim or NCEP Reanalysis data.

% subsubsection greb (end)

\subsubsection{Hector} % (fold)
\label{ssub:hector}

Hector is an open-source, object-oriented, reduced-form global climate carbon-cycle model that includes simplified but physically-based representations of the most critical global-scale Earth system processes \citep{Hartin_2015_f7bz77, Vega-Westhoff_2019_ggftgq}.
At each annual time step, Hector calculates a global change in radiative forcing from changes in atmospheric composition, and then passes this forcing to its climate core.
The carbon cycle consists of three parts: a one-pool atmosphere, three-pool land, and four-pool ocean.
The three terrestrial carbon cycle pools --- vegetation, detritus, and soil --- exchange carbon with the atmosphere through primary production (which increases with atmospheric \chem{CO_2} concentration) and heterotrophic respiration (which increases with temperature).
Hector actively solves the inorganic carbon system in the surface ocean, directly calculating air-sea fluxes of carbon and ocean pH.
A basic representation of thermohaline circulation drives heat and carbon exchange between low- and high-latitude surface ocean pools, an intermediate ocean pool, and a deep ocean pool.
Besides \chem{CO_2}, Hector also includes a basic representation of the atmospheric chemistry of halocarbons, \chem{O_3}, NOx, and \chem{CH_4}.
The climate core of Hector is based on DOECLIM \citep{Kriegler2005,  Tanaka2007aggregated} and involves an analytical solution to ordinary differential equations governing heat exchange between the atmosphere and land, as well as a one-dimensional ocean heat diffusion model.
Hector is written in C++ and can be run as a standalone executable, coupled to an integrated assessment model, as well as via a Python \citep{Willner_2017_gftgkk} or R package.
Hector’s flexibility, open-source nature, and modular design facilitates a broad range of research in various areas.

Four Hector climate parameters, $S$, $\kappa$ (ocean heat diffusivity), $\alpha_a$ (aerosol forcing scaling factor), and $\alpha_v$ (volcanic forcing scaling factor) were calibrated to six individual CMIP6 ESMs.
The calibration protocol implemented followed \citet{Dorheim_2019_Under_Review} that utilises concentration-driven historical temperature and future multi-forcing scenario temperature and ocean heat flux as comparison data in a nonlinear optimization calibration.
For the `probabilistic' Hector simulations in this study, we used 1000 random draws from joint posterior parameter distributions from the Bayesian calibration of Hector against historical observations in \citet{Vega-Westhoff_2019_ggftgq}.

% subsubsection hector (end)

\subsubsection{Held et al. two layer model} % (fold)
\label{ssub:held_et_al_two_layer_model}

The Held et al. two layer model (University of Melbourne implementation) is based on \citet{Held_2010_bjj8vh} with a state-dependent feedback factor following \citet{Rohrschneider_2019_ggfx33}.
It uses a two-layer ocean model to simulate the temperature response to (effective) radiative forcing.

The Held two layer model is provided in a default setup which broadly reproduces historical GSAT observations with an equilibrium climate sensitivity of 3\unit{\degree C}.
It has also been calibrated to the idealised \chem{CO_2}-only experiments (abrupt-2xCO2, abrupt-4xCO2, abrupt-0p5xCO2, 1pctCO2) of a number of CMIP6 models.
Only these calibrations are reported as emulating CMIP6 models for experiments including non-\chem{CO_2} climate drivers requires model specific estimates of non-\chem{CO_2} radiative forcings, which were not available during the calibration process.
Throughout the calibrations, a default forcing due to a doubling of atmospheric \chem{CO_2} of 3.74 \unit{W m^{-2}} was chosen for simplicity.
Using CMIP6 model specific values would marginally improve the fits and the correspondence between fitted parameters and ESM properties.

% subsubsection held_et_al_two_layer_model (end)

\subsubsection{MAGICC} % (fold)
\label{ssub:magicc}

MAGICC, the `Model for the Assessment of Greenhouse Gas Induced Climate Change' has its origins in early upwelling-diffusion ocean parameterisations by \citet{Wigley_1985}, with components for sea level rise \citep{Wigley_1987} and a carbon cycle being added subsequently \citep{Wigley_1991}.
MAGICC has been used frequently in all past IPCC reports, often in conjunction with the ISAM \citep{jain1994integrated} and Bern-CC \citep{Siegenthaler_1992} carbon cycle models.
Initially, MAGICC was written in Fortran 77, then transferred to Fortran 90 in the 2000s \citep{Meinshausen2011a, Meinshausen2011b}, amended by a permafrost module \citep{Schneider_von_Deimling_2012} and a new sea level rise module \citep{Nauels_2017}.
An updated version (MAGICC7.0.0) was used to generate the GHG concentration projections that are the input assumptions for the CMIP6 ESM experiments \citep{Meinshausen_2019_gf86j8}, mainly in ScenarioMIP, but also in AerChemMIP and PMIP4.
MAGICC7.0.0 also included several updated radiative forcing parameterisations, including a \chem{CO_2}, \chem{CH_4} and \chem{N_2O} parameterisation that capture results by \citet{Etminan_2016_gdkwbj}, while allowing for a wider range of input concentrations (see \citet{Meinshausen_2019_gf86j8} for details).

For the calibration to individual CMIP6 models, three key advances have been made since MAGICC7.0.0.
Firstly, a land heat capacity term was included.
Previously, only the ocean provided heat capacity in the energy balance model with ocean heat uptake equalling the parameterised radiative flux imbalance at the top of the atmosphere.
Secondly, the parameterisation of the sensitivity of climate sensitivity to the overall forcing magnitude has been adapted.
Previously, this sensitivity was implemented as an independent scaling of land and ocean feedback parameters \citep{Meinshausen2011a}.
In MAGICC7.1.0, there are two parameterisations that allow for a time-changing effective climate sensitivity - apart from the geometric effect that arises from constant land and ocean feedback parameters but time-changing ratios of land and ocean warming.
The first is an option for the calibration routine to allow a linear adjustment of the climate sensitivity with a forcing change, pegged at doubled \chem{CO_2} concentration.
This immediate adjustment of the climate sensitivity is independent from the second one, which allows for a slow and inert response, such as to parameterise the slow change of albedo with melting ice caps.
This second sensitivity adjustment is assumed to be proportional to the cumulative temperature over a window of several hundred years.
Given the wide range of different experiments in the CMIP6 archives, ranging from highly idealized scenarios to multi-gas 21\textsuperscript{st} century results, MAGICC’s calibration routines (which calibrate to all available scenarios, both idealised, and multi-gas scenarios at once) can differentiate between immediate and inert climate sensitivity adjustment terms.

% subsubsection magicc (end)

\subsubsection{MCE} % (fold)
\label{ssub:mce}

MCE, a minimal CMIP emulator, is a tool for diagnosing and emulating the global behavior of complex AOGCMs with minimally required parameters for both simplicity and accuracy.
It consists of a thermal response module and a carbon cycle module.
The thermal response, described in \citet{Tsutsui_2017_f9nsgw}, is represented by the sum of three exponentials as an impulse response function to effective radiative forcing, assuming a constant climate feedback parameter.
The forcing model uses logarithmic scaling in terms of atmospheric \chem{CO_2} concentrations and its amplification from the first to the second doubling.
Non-\chem{CO_2} forcing is not explicitly considered.
The carbon cycle is based on \citet{Hooss_2001_ct8pxp} and \citet{Joos_1996_ggfk4m} and additionally incorporates simple climate-carbon cycle feedback for the chemical equilibrium of the marine inorganic carbon cycle and the decay of organic materials in the terrestrial biosphere.


The thermal response and forcing parameters are calibrated to temperature and heat uptake timeseries from abrupt-4xCO2 and 1pctCO2 experiments of a target AOGCM, from which ECS and TCR are diagnosed \citep{Tsutsui_2019_Submitted}.
The diagnosed ECS is properly scaled down from an estimated equilibrium temperature increase to a quadrupling forcing level.
The current calibration includes individual model emulation for 25 CMIP5 models and 22 CMIP6 models, and five representative sets adjusted to nominal quantiles of 0.17, 0.33, 0.5, 0.67, and 0.83 in terms of transient climate response to cumulative \chem{CO_2} emissions (TCRE) based on the likely range assessed in the IPCC Fifth Assessment Report \citep{IPCC_2013_WGI_Ch_12}.
The carbon cycle parameters are crudely calibrated to historical carbon balance assessed in AR5 \citep{IPCC_2013_WGI_Ch_6}, and mean properties of CMIP5 ESMs \citep{Arora_2013_f45ckj}, and are fixed across all model configurations.
The TCRE-based median calibration is close to the current default configuration, which is defined as a CMIP5 mean.

% subsubsection mce (end)

\subsubsection{OSCAR} % (fold)
\label{ssub:oscar}

OSCAR v3.1 is an open-source reduced-form Earth system model, whose modules mimic models of higher complexity in a probabilistic setup \citep{Gasser_2017_f9qrcj}.
The response of the global surface temperature to radiative forcing follows a two-box model formulation \citep{Geoffroy_2013_f4sjgr}.
OSCAR calculates the radiative forcing caused by greenhouse gases (\chem{CO_2}, \chem{CH_4}, \chem{N_2O}, 37 halogenated compounds), short-lived climate forcers (tropospheric and stratospheric ozone, stratospheric water vapor, nitrates, sulfates, black carbon, primary and secondary organic aerosols) and changes in surface albedo.
The ocean carbon cycle is based on the mixed-layer response function of \citet{Joos_1996_ggfk4m}, albeit with an added stratification of the upper ocean derived from CMIP5 \citep{Arora_2013_f45ckj} and with an updated carbonate chemistry.
The land carbon cycle is divided into five biomes and five regions, and each of the 25 biome/region combinations follows a flexible three-box model (soil, litter and vegetation).
The pre-industrial state of the land carbon cycle is calibrated to TRENDYv7 \citep{Quere_2018_gfn48b} and its transient response to \chem{CO_2} and climate is calibrated to CMIP5 \citep{Arora_2013_f45ckj}.
Land cover change, wood harvest and shifting cultivation are also accounted for, thanks to a dedicated book-keeping module that allows OSCAR to estimate its own \chem{CO_2} emissions from land-use change \citep{Gasser_2017_f95tx7}.
Permafrost thaw and the consequent emission of \chem{CO_2} and \chem{CH_4} is also modeled \citep{Gasser_2018_gd6h9w}.
\chem{CH_4} emissions from wetlands are calibrated on WETCHIMP \citep{Melton_2013_f4nv56}.
In addition, biomass burning emissions are calculated endogenously following the book-keeping module and the wildfire feedback.
These emissions were therefore subtracted from the input RCMIP data used to drive OSCAR to avoid double counting.
The atmospheric lifetimes of non-\chem{CO_2} greenhouse gases are impacted by non-linear tropospheric \citep{Holmes_2013_f4knpr} and stratospheric \citep{Prather_2015_f7h2dr} chemistry.
Tropospheric ozone follows the formulation by \citet{IPCC_2001_WG1_Ch_4} but recalibrated to ACCMIP \citep{Stevenson_2013_f3cvx2}.
Stratospheric ozone is derived from \citet{Newman_2006_cdmtbj} and \citet{Ravishankara_2009_b9zwkv}.
Aerosol-radiation interactions are based on CMIP5 and AeroCom2 \citep{Myhre_2013_f4q383}, and aerosol-cloud interactions depend on the hydrophilic fraction of each aerosol and follow a logarithmic formulation \citep{Hansen_2005_dvdpr9}.
Surface albedo change induced by land-cover change follows \citet{Bright_2013_f5kz3t}.
The one induced by deposition of black carbon on snow is calibrated on ACCMIP globally \citep{Lee_2013_gb8rd9} and then regionalized \citep{Reddy_2007_btxwm8}.

In this project, 10000 elements of a Monte-Carlo ensemble were generated, and each simulation was run using all these configurations.
Each configuration was then weighted by comparing its results in the concentration-driven historical simulation to observations.
Four observations were used: (i) the increase in global surface temperature from 1850-1900 to 2006-2015 \citep{IPCC_2018_SR15_Ch_1}, (ii) the cumulative ocean heat sink over 1959-2017, (iii) the cumulative fossil-fuel \chem{CO_2} emissions over 1959-2017, and (iv) the average fossil-fuel \chem{CO_2} emissions over 2000-2009 \citep{Quere_2018_gfn48b}.
All final outputs are provided as the resulting weighted means and standard deviations.
This constraining setup leads to an equilibrium climate sensitivity of $2.64 \pm 0.46$ \unit{\degree C}. We did not further constrain our ensemble.


% subsubsection oscar (end)

\subsubsection{WASP} % (fold)
\label{ssub:wasp}

The Warming Acidification and Sea level Projector (WASP) is an efficient Earth system model capable of producing a large ensemble of simulations on a standard desktop computer.
WASP comprises an 8-box representation of the coupled atmosphere-ocean-land system \citep{Goodwin_2016_gft5nc}.
The atmosphere comprises a single box connected to the land and ocean systems.
The land system comprises a vegetation carbon box with \chem{CO_2} and temperature dependent net primary production, and a soil carbon box with temperature dependent carbon respiration.
The ocean comprises a surface mixed layer box, exchanging heat and carbon with 4 sub-surface boxes \citep{Goodwin_2016_gft5nc}.
The exchange of carbon dioxide between the atmosphere and surface ocean boxes is solved via a buffered carbon inventory approach \citep{Goodwin_2014_gc3f5c}, avoiding the computational expense of iteratively solving seawater carbonate chemistry.
Global mean surface warming is solved via the extended energy balance equation of \citep{Goodwin_2014_gc3f5c}.
The version of WASP used here adopts time-evolving climate feedbacks as described in \citet{Goodwin_2018_gfhdmr}, with separate climate feedback terms representing processes such as cloud feedback, water vapour-lapse rate feedback and surface albedo feedback.
The WASP model is coded in C++, with the code for the version used here available from the supplementary material of \citet{Goodwin_2018_gfhdmr}.

Initially, a 10 million member ensemble is generated with the values of 21 input parameters varied independently between simulations.
The 21 input parameter distributions are as described in \citet{Goodwin_2018_gfhdmr}, except that here: (i) the surface ocean carbon uptake e-folding timescale is varied from a uniform random distribution between the limits of 0.5 years and 1.0 years, and (ii) the sensitivity of soil carbon residence timescale to temperature is varied from a random-normal distribution with mean -1.36 yr K\textsuperscript{-1} and standard deviation 0.45 yr K\textsuperscript{-1}, to match the variation seen in Dynamic Global Vegetation Models used in \citet{Pugh_2018_gfpzsd}.
Each simulation is integrated from years 1765 to 2018 using historical forcing.
All 10 million initial ensemble members are then assessed against an observational consistency test \citep{Goodwin_2018_gfhdmr} using the methodology described in \citet{Goodwin_2018_gczw79}.
The observational consistency test comprises assessment of global surface warming, sea surface temperature warming, ocean heat uptake, ocean carbon uptake and land carbon uptake over various historical periods, resulting in observation consistent constraints on the ranges of physical climate feedbacks \cite{Goodwin_2018_gfhdmr} and land carbon feedbacks \citep{Goodwin_2019_ggfp6s}.
Around 2000 simulations pass the observational consistency test and are accepted into the final observation-constrained model ensemble.
This final model ensemble thus contains the small number of parameter value combinations that produce observation-consistent simulations, in which the values of the 21 input parameters are no longer independent of one another.
The observation-constrained simulations are integrated into the future for the SSP scenarios (using non-\chem{CO_2} radiative forcing following the Effective Radiative Forcing from Shared Socioeconomic Pathways available at \url{https://doi.org/10.5281/zenodo.3515339}).
To perform the idealised scenarios, that do not have a historically forced period with which to extract the final ensemble, the final observation-consistent model ensemble generated from the ssp585 scenario is used.
Due to the observational constraints used for surface warming, the WASP model’s primary global surface temperature anomaly variable represents a blended land and ocean temperature (GMST).
To calculate surface air temperature (GSAT) for the RCMIP results, the GMST is divided by 0.95 \citep{Cowtan_2015_f7p2js}.

% subsubsection wasp (end)

% subsection participating_models (end)
