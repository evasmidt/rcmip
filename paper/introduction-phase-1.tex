Sufficient computing power to enable running our most comprehensive, physically complete climate models for every application of interest is not available.
Thus, for many applications, less computationally demanding approaches are used.
One common approach is the use of reduced complexity climate models (RCMs), also known as simple climate models (SCMs).

RCMs are designed to be computationally efficient tools, allowing for exploratory research and have smaller spatial, if any, and temporal resolution than complex models.
Typically, they describe highly parameterised macro properties of the climate system.
Usually this means that they simulate the climate system on a global-mean, annual-mean scale although some RCMs even use coarse resolution spatial grids and monthly time-steps.
As a result of their highly parameterised approach, RCMs can be on the order of a million or more times faster than more complex models (in terms of simulated model years per unit CPU time).

The computational efficiency of RCMs means that they can be used where computational constraints would otherwise be limiting.
For example, in the hierarchy of climate models - RCMs, the Earth System Models of intermediate complexity (EMICs) and Earth System Models (ESMs) - it is only RCMs that are sufficiently efficient for large probabilistic ensembles for hundreds of scenarios.
In addition, some Integrated Assessment Models (IAMs) require iterative climate simulations.
In such cases, only RCMs are computationally feasible because hundreds to thousands of climate realisations must be integrated by the IAM for a single scenario to be produced.
RCMs also enable the exploration of interacting uncertainties from multiple parts of the climate system or the constraining of unknown parameters by combining multiple lines of evidence in an internally consistent setup.
In the context of the Assessment Reports of the Intergovernmental Panel on Climate Change (IPCC), a prominent example is the climate assessment of emission scenarios by IPCC Working Group 3 (WGIII).
Hundreds of emission scenarios were assessed in the IPCC’s Fifth Assessment Report (AR5, see \citet{IPCC_AR5_WG3_Ch6}) as well as its more recent Special Report on Global Warming of 1.5\degree C (SR1.5, see \citet{IPCC_2018_SR15_Ch_2,SR15_database}).
(Scenario data is available at \url{https://secure.iiasa.ac.at/web-apps/ene/AR5DB} and \url{https://data.ene.iiasa.ac.at/iamc-1.5c-explorer/} for AR5 and SR1.5 respectively, both databases are hosted by the IIASA Energy Program).
For the IPCC’s forthcoming Sixth Assessment Report (AR6), it is anticipated that the number of scenarios will be in the several hundreds to a thousand (for example, see the full set of scenarios based on the SSPs at \url{https://tntcat.iiasa.ac.at/SspDb}).
Both the number of scenarios and the tight timelines of the IPCC assessments render it infeasible to use the world’s most comprehensive models to estimate the climate implications of these IAM scenarios.

\subsection{Evaluation of reduced complexity climate models} % (fold)
\label{sub:evaluation_of_rcms}

The validity of the RCM approach rests on the premise that RCMs are able to replicate the behaviour of the Earth system and response characteristics of our most complete models.
Over time, multiple independent efforts have been made to evaluate this ability.
In 1997, an IPCC Technical Paper \citep{houghton_1997_adf9ss}, investigated the simple climate models used in the IPCC Second Assessment Report and compared their performance with idealised Atmosphere-Ocean General Circulation Model (AOGCM) results.
Later, \citet{Vuuren_2009_bcmqjh} compared the climate components used in IAMs, such as DICE \citep{Nordhaus_2014} and FUND \citep{waldhoff_2011_marginal}.
\Citet{Vuuren_2009_bcmqjh} also included the RCM MAGICC \citep[version 4 at the time, ][]{Wigley_2001}, which was used in several IAMs.
They focused on five \chem{CO_2}-only experiments to quantify the differences in the behaviour of the RCMs used by each IAM.
\Citet{Harmsen_2015_f72ghq} extended the work of \citet{Vuuren_2009_bcmqjh} to consider the impact of non-\chem{CO_2} climate drivers in the RCPs.
Recently, \citet{Schwarber_2019_ggfp6r} proposed a series of impulse tests for simple climate models in order to isolate differences in model behaviour under idealised conditions.

Despite these efforts, the RCM community does not yet have a systematic, regular intercomparison effort.
This led to the following statement in SR1.5 \citep{IPCC_2018_SR15_Ch_2SM}, `The veracity of these reduced complexity climate models is a substantial knowledge gap in the overall assessment of pathways and their temperature thresholds.’
This study provides a first step to fill this gap via a systematic intercomparison.
A systematic intercomparison is also likely to provide other benefits, similar to those that the AOGCM and ESM modelling communities have gained over multiple iterations of CMIP \citep{carlson_eyring_2017}.
Developing a systematic comparison for RCMs will provide similar benefits to the RCM community including building a community of reduced complexity modellers, facilitating comparison of model behaviour, improving understanding of RCMs' strengths and limitations, and ultimately improving RCMs.

An ongoing comprehensive evaluation and assessment of RCMs requires an established protocol.
The Reduced Complexity Model Intercomparison Project (RCMIP) proposed here provides such a protocol (also see \url{rcmip.org}).
In the RCMIP community call (available at rcmip.org) RCMs were broadly defined as follows: ``[...] RCMIP is aimed at reduced complexity, simple climate models and small emulators that are not part of the intermediate complexity EMIC or complex GCM/ESM categories.''
In practice, we encouraged any group in the scientific community who identifies with the label of RCM to participate in RCMIP, see Table \ref{tab:rcmip-model-overview} for an overview of the models which participated in RCMIP Phase 1.

We aim for RCMIP to provide a focal point for further development and an experimental design which allows models to be readily compared and contrasted, mirroring the regular comparisons which are performed for AOGCMs and ESMs in each of CMIP’s iterations.
We intend for RCMIP to faciliate more regular and targeted assessment of RCMs.

Thus, while RCMIP mirrors many of the experimental setups developed within CMIP6, RCMIP focuses on RCMs and is hence not one of the official CMIP6 \citep{eyring_2016_fjksdl} endorsed intercomparison projects (that are instead targeted at ESMs).
Nonetheless, RCMs are part of the climate model hierarchy so we aim to make comparing the RCMIP results with results from other modelling communities, specifically CMIP, as simple as possible.
Accordingly, RCMIP replicates selected experimental designs of many of the CMIP-endorsed MIPs, particularly the DECK \citep{eyring_2016_fjksdl} and ScenarioMIP \citep{ONeill_2016_f87n56} simulations.

In what follows, we describe RCMIP Phase 1.
In section \ref{sec:science-objectives}, we detail the domain of RCMIP Phase 1 and its research questions.
In section \ref{sec:participating_models_and_their_configuration}, we provide an overview of the participating models and their configuration.
In section \ref{sec:simulation-design}, we describe the experimental setup.
In section \ref{sec:sample-results} we present results from RCMIP Phase 1, before presenting possible extensions in section \ref{sec:extensions} and conclusions in section \ref{sec:conclusions}.

% subsection evaluation_of_rcms (end)
