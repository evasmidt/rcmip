RCMs generally model multiple steps in the emissions-climate change cause-effect chain including gas cycles (emissions to concentration step), radiative forcing parameterisations (concentrations to radiative forcing step) and temperature response (radiative forcing to warming step).
Here, effective radiative forcing and radiative forcing are defined following \citet{IPCC_2013_WGI_Ch_8}.
In contrast to radiative forcing, effective radiative forcing includes rapid adjustments beyond stratospheric temperature adjustments thus is a better indicator of long-term climate change.

Each point in the chain can be used as the starting point for simulations i.e. the simulation might be defined in terms of prescribed concentrations, emissions or radiative forcing.
In Phase 1 of RCMIP, we focus on experiments which are defined in terms of concentrations to facilitate a direct comparison with CMIP experiments, most of which are also defined in terms of concentrations.

RCMIP Phase 1 focuses on 19 experiments, which can be broken down into two categories: scenario-based and idealised.
We provided all inputs following, and requested all outputs follow, a standard format to facilitate ease of data analysis and re-use (Supplementary Section S1).
This common data format was developed for RCMIP and combines elements of the integrated assessment community standard \citep{Gidden_2019_ggfx68} and the CMIP6 definitions of variables and scenarios.

\subsection{Scenario based experiments} % (fold)
\label{sub:scenario_experiments}

Scenario based experiments examine model responses to historical transient forcing as well as a range of future scenarios.
The historical experiments provide a way to compare RCM output against observational data records (Research Question 2), and are complementary to the idealised experiments (Section \ref{sub:idealised_experiments}) which provide a cleaner assessment of model response to forcing.
The future scenarios probe RCM responses to a range of possible climate futures, both continued warming as well as stabilisation or overshoots in forcing.
The variety of scenarios is a key test of model behaviour, evaluating them over a range of conditions rather than only over the historical period.
Direct comparison with CMIP output then provides information about the extent to which the simplifications involved in RCM modelling are able to reproduce the response of the  most advanced, physically-based ESMs (Research Question 3).

RCMIP Phase 1's scenario experiments are: historical, ssp119, ssp126, ssp245, ssp370, ssp434, ssp460, ssp534-over, ssp585, rcp26, rcp45, rcp60 and rcp85.
We focus on simulations (historical plus future) which cover the range in forcing scenarios from the CMIP6 ScenarioMIP exercise \citep{ONeill_2016_f87n56,Riahi_ssps_2017} and CMIP5 RCP scenarios \citep{vanVuuren_rcps_2011}.
These quickly reveal differences in model projections over the widest available scenario range which can also be compared to CMIP6 output.
The CMIP5 experiments are particularly useful as they provide a direct comparison between CMIP5 and CMIP6 scenarios (Research Question 4), something which has only been done to a limited extent with more complex models \citep{wyser_2019_scojd2}.

All of these experiments are defined in terms of concentrations of well-mixed greenhouse gases.
Here, `well-mixed greenhouse gases' refers to \chem{CO_2}, \chem{CH_4}, \chem{N_2O}, hydrofluorocarbons (HFCs), perfluorocarbons (PFCs) and hydrochlorofluorocarbons (HCFCs).
However, scenario experiments include more than just well-mixed greenhouse gases so these concentrations are supplemented by aerosol precursor species emissions, ozone-relevant emissions and natural effective radiative forcing variations.
Here, `aerosol precursor species emissions’ refers to emissions of sulfur, nitrates, black carbon, organic carbon and ammonia.
‘Ozone-relevant emissions’ refers to emissions of carbon monoxide and non-methane volatile organic compounds (NMVOCs).
For models which do not include the aerosol emissions to effective radiative forcing or ozone-relevant emissions to ozone effective radiative forcing steps, prescribed effective radiative forcings can instead be used.
Here ‘natural effective radiative forcing variations’ refers to effective radiative forcing due to natural volcanic eruptions and changes in solar irradiance.
All data sources are described in Supplementary Section \ref{sub:scenario_based_experiments_data_sources}.

The key difference between the RCMIP experiments and the CMIP experiments is that some RCMs include more anthropogenic drivers than CMIP models.
Specifically, CMIP models do not include the full range of HFC, PFC and HCFC species, instead using equivalent concentrations \citep{Meinshausen_CMIP6_concs_2017,Meinshausen_2019_gf86j8}.
In addition, some CMIP models will not include the effect of aerosol precursors such as nitrates, ammonia and organic carbon \citep{mccoy_aerosol_cloud_2017}.

% subsection scenario_experiments (end)

\subsection{Idealised experiments} % (fold)
\label{sub:idealised_experiments}

In addition to the scenario-based experiments, RCMIP Phase 1 also includes a number of idealised experiments.
All of these experiments are defined in terms of \chem{CO_2} concentrations alone.
These experiments provide an easy point of comparison with output from other models, particularly CMIP output, as well as information about basic model behaviour and dynamics which can be useful for understanding the differences between models.

RCMIP Phase 1’s idealised experiments are: 1pctCO2, 1pctCO2-4xext, abrupt-4xCO2, abrupt-2xCO2 and abrupt-0p5xCO2.
These examine the RCMs' response to a one percent per year increase in atmospheric \chem{CO_2} concentrations (1pctCO2), 1pctCO2 followed by constant \chem{CO_2} concentrations once atmospheric \chem{CO_2} concentrations quadruple (1pctCO2-4xext) and abrupt changes in atmospheric \chem{CO_2} to four times pre-industrial levels (abrupt-4xCO2), double pre-industrial levels (abrupt-2xCO2) and half pre-industrial levels (abrupt-0p5xCO2) - mirroring the respective CMIP experiments \citep{eyring_2016_fjksdl}.

The experiments reveal differences in model response to forcing, particularly whether the RCM response to forcing includes non-linearities.
In addition, these experiments also provide a direct comparison with CMIP experiments (i.e. more complex model behaviour) and are a key benchmark when examining an RCM's ability to emulate more complex models (Research Question 3).
In these concentration-driven experiments, RCMs report emissions (often referred to as `inverse emissions’) and carbon cycle behaviour consistent with the prescribed \chem{CO_2} pathway.
These inverse emissions are key to exploring the variation in the relationship between surface air temperature change and cumulative emissions of \chem{CO_2} \citep{allen2009warming,matthews2009proportionality,Meinshausen_2009_b9j8fj,zickfeld2009setting} over a range of models and parameter values (Research Question 5).

% subsection idealised_experiments (end)

\subsection{Output Variables} % (fold)
\label{sub:variables}

Phase 1 of RCMIP focuses on five key output variables.
The focus on a limited set allows us to discern major differences between RCMs and provides insights into the reasons for such differences.
The first variable of interest is surface air temperature change.
We choose this variable because it is comparable to available observations and CMIP output and is also policy-relevant.

In addition to surface air temperature change, we request total, anthropogenic, \chem{CO_2} and aerosol effective radiative forcing.
These forcing variables are key indicators of the long-term drivers of climate change within each model as well as being key metrics for the IAMC community.
In particular, aerosol effective radiative forcing is highly uncertain and a key source of difference between RCMs.

The final variable we request is \chem{CO_2} emissions.
Given that all our experiments are defined in terms of concentrations, we request \chem{CO_2} emissions compatible with the prescribed \chem{CO_2} pathways.

% subsection variables (end)
