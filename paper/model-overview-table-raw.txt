Model (acronym used in figures)
Spatial resolution
Temporal resolution
Climate response to radiative forcing (upwelling diffusion ocean, impulse response etc.)
Other components (e.g. carbon cycle, sea-level rise, ocean pH)
ACC2 (ACC2)
Global land/ocean
Annual
1D ocean heat diffusion (DOECLIM)
Land and ocean carbon cycle, ocean carbonate chemistry, parameterized atmospheric chemistry involving CH4, OH, NOx, CO, and VOC, radiative forcing of 29 halocarbons
AR5IR (ar5ir-2box, ar5ir-3box)
Global
Annual
Impulse response
None
CICERO-SCM (CICERO-SCM)
By hemisphere
Annual
Energy balance/upwelling diffusion model
Land and ocean carbon cycle
ESCIMO (ESCIMO)
Global
Annual
Conserved flows of carbon, heat, albedo, permafrost, biome and biomass change. Driven by GHG emissions, the rest is endogenous.
No complete water cycle, water is tracked as ocean, high and low clouds, ice (glacial, arctic, Greenland and Antarctic), and vapor.
FaIR (FaIR)
Global
Annual
Modified impulse response
Simple ozone, aerosol, greenhouse gas and land use relationships from precursor emissions
GIR (GIR)
Global
Annual
Modified Impulse Response
Simple (typically state-dependent) ozone, aerosol and greenhouse gas relationships
GREB (GREB-v1-0-1)
96 x 48 grid
Monthly
Energy Balance model atmospheric transport of heat and moisture, surface and subsurface ocean layer.
Hydrological cycle, sea ice.
Hector (hector|62efg3)
Global
Annual
1D ocean heat diffusion (DOECLIM)
Land and ocean carbon cycle. Ocean carbonate chemistry and simplified thermohaline circulation. Atmospheric chemistry of CH4, OH, NOx, and halocarbons.
Held et. al two layer model (held-two-layer-uom)
Global
Monthly
Two-layer ocean with state-dependent climate feedback factor
None
MAGICC (MAGICC7.1.0.beta)
Land/ocean by hemisphere.
Annual
Atmospheric energy balance model with 50-layer upwelling-diffusion-entrainment ocean.
Carbon cycle, permafrost module, ozone, 42 greenhouse gas cycles, sea level rise.
MCE (MCE)
Global
Flexible; typically annual or five-yearly
Impulse response
Land and ocean carbon cycle
OSCAR (OSCAR-v3-0)
Global, with regionalized land carbon cycle
annual
Impulse response
Ocean and land carbon cycle, book-keeping module for land-use, biomass burning, wetlands, permafrost, tropospheric and stratospheric chemistry, 37 halogenated compounds, aerosols
WASP (WASP)
Global
Annual
Energy balance using time evolving climate feedback, with conservation of heat and carbon
Land and ocean carbon cycle. Ocean pH. Thermosteric and ice-melt sea level components available.
