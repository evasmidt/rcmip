The key point of this paper is to introduce RCMIP, its goals and its setup.
As a proof of concept, we also include key initial research questions, the implemented experimental setup and associated results from RCMIP’s first phase.

\textbf{Research question 1: Is the reduced complexity modelling community ready to run an intercomparison and how long would such an intercomparison take to run?}

Model intercomparisons require significant effort on the part of the organising community and each of the modelling teams involved.
The reduced complexity modelling community has not undertaken such an effort previously, hence the first question is whether the community is ready to perform an intercomparison.

In addition to whether an intercomparison is possible, the second part of the first question is how long and how much effort is required to perform the intercomparison.
The most successful intercomparisons are built on standardised protocols for experiment design, model setup and data handling. To date, no such standards exist for the reduced complexity modelling community.

Here we investigate how easily the benefits of systematic intercomparison can be brought to the reduced complexity modelling community by performing the first of many envisaged rounds of intercomparison. In the process, we gain vital insights into the effort, timelines and scope which can reasonably be managed by the participating modelling teams. Such knowledge is vital for planning future efforts.

\textbf{Research question 2: Can reduced complexity climate models capture observed historical global-mean surface air temperature (GSAT) trends?}

The second research question focuses on a key metric for evaluating RCMs against observations.
This research question evaluates the extent to which each RCM’s approximations and parameterisations cause its response to deviate from observational data.

However, given the limited amount of observations available, comparing only with observations leaves us with little understanding of how RCMs perform in scenarios apart from a historical one in which anthropogenic emissions are heating the climate.
Recognising that there are a range of possible futures, it is vital to also assess RCMs in other scenarios.
Prominent examples include stabilising or falling anthropogenic emissions, strong mitigation of non-\chem{CO_2} climate forcers and scenarios with \chem{CO_2} removal.
The limited observational set motivates RCMIP's third research question: evaluation against more complex models.

\textbf{Research question 3: To what extent can reduced complexity models emulate the global-mean temperature response of more complex models?}

Whilst the response of more comprehensive models may not represent the behaviour of the actual Earth System, they are the best available representation of our understanding of the Earth System's physical processes.
By evaluating RCMs against more complex models, we can quantify the extent to which the simplifications made in RCMs limit their ability to capture physically-based model responses.
For example, the extent to which the approximation of a constant climate feedback in some RCMs limits the RCM's ability to replicate ESMs' longer-term response under either higher forcing or lower overshoot scenarios \citep{Rohrschneider_2019_ggfx33}.

\textbf{Research question 4: What can a multi-model ensemble of RCMs tell us about the difference between the SSP-based and RCP scenarios?}

The SSP-based scenarios \citep{ONeill_2016_f87n56,Riahi_ssps_2017} are the cornerstone of CMIP6’s ScenarioMIP and are an update of CMIP5’s RCP scenarios \citep{vanVuuren_rcps_2011}.
One of the key intents behind some of the SSP-based scenarios is that they share the same nameplate 2100 radiative forcing level as the RCPs (e.g. ssp126 and rcp26, ssp245 and rcp45), the idea being that they would have similar climatic outcomes despite their different atmospheric concentration inputs.
However, the nameplate radiative forcing comparisons between RCPs and SSPs were undertaken on the basis of IPCC AR5-consistent stratospheric-adjusted radiative forcings \citep{IPCC_2013_WGI_Ch_8}.
Taking into account new insights into respective \chem{CO_2} and \chem{CH_4} forcings, as well as effective radiative forcings, different climate responses can be expected.
In fact, \citet{Wyser_2020} suggest that the difference in atmospheric concentrations results in non-trivial differences in climate projections.

Unfortunately, evaluating the scenario differences between RCPs and SSP-based scenarios with a large, identical set of CMIP models is difficult because of the computational cost (many CMIP6 modelling groups will not perform all CMIP6 ScenarioMIP experiments, let alone performing extra CMIP5 experiments).
With an ensemble of RCMs, we can provide further insight into how much the change in emissions pathways affects climate projections using identical models, building on the insights from the CMIP groups which can afford to run the required experiments.
In addition, RCMs also offer one other benefit: they can diagnose effective radiative forcing directly.
As a result, RCMs can provide more detailed insights into the reasons for differences because they provide a more detailed breakdown of the emissions-climate change cause-effect chain.
In contrast, diagnosing effective radiative forcing from CMIP models is a difficult task which requires a number of extra experiments, all of which come at additional computational cost \citep{smith_cmip6erf_2020}.

\textbf{Research question 5: How does the relationship between cumulative CO\textsubscript{2} emissions and global-mean temperature vary both between RCMs and within a parameter ensemble of an RCM?}

The relationship between cumulative \chem{CO_2} emissions and global-mean temperature is key to deriving the transient climate response to emissions \citep{IPCC_2018_SR15_Glossary}, a key metric in the calculation of our remaining carbon budget \citep{Rogelj_2019_gf45fs}.
Here we investigate how this relationship varies between RCMs and within a parameter ensemble from a given RCM.
While a multi-model ensemble demonstrates variance due to model structure, the parameter ensemble demonstrates variance that arises solely as a result of changes in the strength of the response of individual components.
These insights build on results from experiments with more complex models \citep[see e.g.][]{arora_2019}, which cannot perform such large perturbed parameter ensembles because of computational cost.
